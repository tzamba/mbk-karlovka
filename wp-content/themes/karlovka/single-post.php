<?php get_header(); ?>
		<div id="main" class="section"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<div id="content">
						<?php 
							// display_breadcrumbs();
							get_template_part('content', 'post');
						?>
					</div>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
				</div></div>
			</div>
		</div></div>
<?php get_footer(); ?>