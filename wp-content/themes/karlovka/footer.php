		<div id="footer" class="section"><div class="wrapper">
			<div id="copy">MBK Karlovka &copy; <?php echo date('Y'); ?> | <a href="/">www.mbkkarlovka.sk</a></div>
			<a onClick="ga('send', 'event', 'Zombi', 'click', 'Zombi redirect', 13);" href="http://www.zombi.sk" id="z" target="_blank"></a>

			<div class="third"><div class="wrapper">
				<?php
					wp_nav_menu(array(
						'theme_location' => 'footer-menu 1',
						'menu' => 'main menu',
						'echo' => true,
						'fallback_cb' => '',
						'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>'
					));
				?>
			</div></div>
			<div class="third"><div class="wrapper">
				<?php
					wp_nav_menu(array(
						'theme_location' => 'footer-menu 2',
						'menu' => 'main menu',
						'echo' => true,
						'fallback_cb' => '',
						'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>'
					));
				?>
			</div></div>
			<div class="third t-white"><div class="wrapper">
				<strong>KONTAKTNÉ INFORMÁCIE</strong>
				<p>
					Mládežnícky basketbalový klub Karlovka<br>
					Strmý vŕšok 34<br>
					841 06 Bratislava<br><br>
					<strong>SEKRETARIÁT KLUBU</strong><br>
					Furdekova 16<br>
					851 04 Bratislava<br>
					<a class="f-right" href="<?php echo home_url('/'); ?>o-klube/kontakt">Kontakt <i class="fa fa-angle-double-left"></i></a>
				</p>
			</div></div>
		</div></div>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script>;window.jQuery || document.write('<script src="lib/jquery/jquery-1.11.1.min.js"><\/script>');</script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/moment.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/jquery/jquery.scrollTo.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/jquery/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fullcalendar.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/swiper/js/idangerous.swiper-2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
		<?php wp_footer(); ?>
	</body>
</html>
<!-- <?php echo get_num_queries(); ?>q / <?php timer_stop(1); ?>s -->