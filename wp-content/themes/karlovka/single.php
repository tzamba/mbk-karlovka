<?php get_header(); ?>
		<div id="main" class="section"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<div id="content">
						<?php
							// display_breadcrumbs();
							get_template_part('content');
						?>
					</div>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<?php
						$postType = get_post_type();
						$ID = get_the_id();
					?>
					<?php if (PTYPE_TIM == $postType) : ?>
						<?php echo do_shortcode('[swiper tim="'.$ID.'"]');?>
					<?php endif;?>
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
				</div></div>
			</div>
		</div></div>
<?php get_footer(); ?>