<?php
/*
Template Name: Hraci
*/
	get_header();
?>
		<div id="main" class="section"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<div id="content">
						<h1><?php the_title(); ?></h1>
						<?php
							$tim = get_post_meta(get_the_id(), 'timy_post_class', true);
							$players = getPlayersByCategory($tim[0]);

							$postR = array();
							$postK = array();
							$postP = array();
							foreach ($players as $player) {
								$post    = get_post_custom_values('wpcf-post', $player->ID);
								switch ($post[0]) {
									case 'r':
										$postR[] = $player;
										break;
									case 'k':
										$postK[] = $player;
										break;
									default:
										$postP[] = $player;
										break;
								}
							}
							$posts = array($postR, $postK, $postP);
						?>
						<div class="content-wrapper">
							<?php if (!empty($players)) : ?>
								<table class="bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Meno</th>
											<th>Ročník</th>
											<th>Post</th>
											<th>Stav</th>
											<th>Škola</th>
											<th></th>
										</tr>
									</thead>
								<?php foreach ($posts as $players) : ?>
									<?php foreach ($players as $player) : ?>
									<?php
										$number  = get_post_custom_values('wpcf-number', $player->ID);
										$name    = get_post_custom_values('wpcf-name', $player->ID);
										$surname = get_post_custom_values('wpcf-surname', $player->ID);
										$yearly  = get_post_custom_values('wpcf-yearly', $player->ID);
										$post    = get_post_custom_values('wpcf-post', $player->ID);
										$state   = get_post_custom_values('wpcf-state', $player->ID);
										$school  = get_post_custom_values('wpcf-school', $player->ID);
										$link    = get_permalink($player->ID);
									?>
										<tr>
											<td><?php echo $number[0]; ?></td>
											<td><?php echo $name[0] . ' ' . $surname[0]; ?></td>
											<td><?php echo $yearly[0]; ?></td>
											<td><?php echo strtoupper($post[0]); ?></td>
											<td><?php echo strtoupper($state[0]); ?></td>
											<td><?php echo (!empty($school)) ? $school[0] : ''; ?></td>
											<td><a href="<?php echo $link; ?>">detail&nbsp;hráča</a></td>
										</tr>
									<?php endforeach; ?>
								<?php endforeach; ?>
								</table>
							<?php endif; ?>
							<?php echo apply_filters('the_content', $post->post_content); ?>
						</div>
					</div>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<?php echo do_shortcode('[swiper tim="'.$tim[0].'"]');?>
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
				</div></div>
			</div>
		</div></div>
<?php get_footer(); ?>