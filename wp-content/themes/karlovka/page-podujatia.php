<?php
/*
Template Name: Podujatia
*/
	get_header();
?>
		<div id="main" class="section"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<div id="content">
						<h1><?php the_title(); ?></h1>
						<div class="content-wrapper">
							<?php
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$args = array('posts_per_page' => 10, 'paged' => $paged, 'post_type' => PTYPE_PODUJATIE, 'orderby' => 'date', 'order' => 'DESC', 'post_status' => 'any');
								$wp_query= new WP_Query($args);

								while ( $wp_query->have_posts() ) :
								$wp_query->the_post();
								$id = $wp_query->post->ID;
							?>
								<article class="article article-horizontal">
									<?php $thumb = get_the_post_thumbnail($id, 'medium'); ?>
									<?php if (!empty($thumb)) : ?>
										<div class="article-image">
											<?php echo get_the_post_thumbnail($id, 'medium'); ?>
										</div>
									<?php endif; ?>
									<header class="header">
										<h2 class="title"><a href="<?php the_permalink(); ?>"><?php echo get_the_title($id); ?></a></h2>
										<small><?php echo strtolower(get_the_date('d. F Y')); ?></small>
										<?php the_excerpt(); ?>
									</header>
								</article>
							<?php
								endwhile;
								wp_pagenavi('<div class="pager">', '</div>');
								wp_reset_query();
							?>
						</div>
					</div>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
				</div></div>
			</div>
		</div></div>
<?php get_footer(); ?>