<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=1024">
		<meta name="robots" content="index,follow">
		<meta name="keywords" content="MBK Karlovka, Bratislava, basketball, basketbalová liga, pohybové a loptové hry, šport">
		<meta name="author" content="Zombi | www.zombi.sk">
		<meta name="google-site-verification" content="SUbl9buGvXAuP1VvcFfddrv13-7ug-oPCb7vXGc1uxg"/>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lib/jquery/css/jquery.mCustomScrollbar.css">
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">
		<title><?php wp_title('|', true, 'right'); ?></title>
		<?php wp_head(); ?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/sk_SK/sdk.js#xfbml=1&appId=264030927116984&version=v2.0";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
		<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-15774041-10', 'auto');ga('send', 'pageview');</script>
		<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-39224305-1', 'auto');ga('send', 'pageview');</script>
	</head>
	<body id="mbk" class="<?php echo (is_404()) ? 'not-found' : get_post_meta( get_the_id(), 'body_class', true); ?>">
		<div id="header" class="section"><div class="wrapper">
			<a href="<?php echo home_url('/'); ?>" id="logo"></a>
			<?php
				wp_nav_menu(array(
					'theme_location' => 'header-menu',
					'menu' => 'main menu',
					'menu_class' => 'top-menu',
					'echo' => true,
					'fallback_cb' => '',
					'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>'
				));
			?>
			<div class="f-right c-right">
				<?php // get_search_form(); ?>
				<div class="socials-mini">
					<!-- <a onClick="ga('send', 'event', 'Link', 'click', 'Social - 2% z dane', 2);" href="/daruj-2-z-dane/" title="Darujte 2% z daní" class="taxes"></a> -->
					<a onClick="ga('send', 'event', 'Link', 'click', 'Social - Become member', 1);" href="/o-klube/klubove-clenstvo/prihlaska-do-kluboveho-clenstva/" title="Prihláška do klubového členstva" class="membership"></a>
					<a onClick="ga('send', 'event', 'Link', 'click', 'Social - Facebook', 3);" href="https://www.facebook.com/mbkkarlovka" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
<!-- 					<a href="#" class="youtube"><i class="fa fa-youtube"></i></a> -->
				</div>
			</div>
		</div></div>