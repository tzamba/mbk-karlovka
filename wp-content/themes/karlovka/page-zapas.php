<?php
/*
Template Name: Zapasy
*/
	get_header();
?>
		<div id="main" class="section"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<div id="content">
						<h1><?php the_title(); ?></h1>
						<?php
//							$teams = get_post_meta(get_the_id(), 'timy_post_class', true);
//							global $wpdb;
//							$sql="SELECT * FROM z_posts WHERE post_type = 'zapas'";
//							$posts = $wpdb->get_results($sql);
//							var_dump($posts);
//						    $matches = getMatchesByCategory((count($teams) > 1) ? null : $teams[0], -1, null, 'DESC');
//							$matches = $categories = array();
//							foreach ($matches as $match) {
//								$taxId = get_post_meta($match->ID, 'timy_post_class', true);
//								$team   = get_post($taxId[0]);
//
//								$cat = $team->post_title;
//								if (!array_key_exists($cat, $categories)) {
//									$categories[$cat] = array();
//								}
//								$categories[$cat][] = $match;
// 								$date = new DateTime($match->post_date);
// 								$date->setTime(13, 13);
// 								$time = $date->format('Y-m-d H:i:s');

// 								set time to 13:13

// 								$cnt = $match->post_content;
// 								$ttl = $match->post_title;
// 								$my_post = array(
// 									'ID'        => $match->ID,
// 									'post_content' => str_replace('Extraliga', 'Eurovia SBL', $cnt),
// 									'post_title'   => str_replace('Extraliga', 'Eurovia SBL', $ttl)
// 								);
// 								wp_update_post( $my_post );
// 								var_dump($match->ID);
//							}
						?>
						<div class="content-wrapper">
							<?php
								$teams = get_post_meta(get_the_id(), 'timy_post_class', true);
								$class = 'active';
								foreach ($teams as $team) {
									echo do_shortcode('[zapasy tim="'.$team.'" filter="false" class="'.$class.'"]');
									$class = '';
								}
							?>
							<?php if (!empty($categories)) : ?>
								<?php foreach ($categories as $name => $category) : ?>
									<h2 class="t-center"><?php echo 'Tím: ' . $name; ?></h2>
									<table class="bordered">
										<thead>
											<tr>
												<th>Dátum</th>
												<th>Zápasy</th>
												<th>Výsledok</th>
												<th></th>
											</tr>
										</thead>
										<?php foreach ($category as $match) : ?>
										<?php
											$home       = get_post_custom_values('wpcf-home', $match->ID);
											$guest      = get_post_custom_values('wpcf-guest', $match->ID);
											$homeScore  = get_post_custom_values('wpcf-home-score', $match->ID);
											$guestScore = get_post_custom_values('wpcf-guest-score', $match->ID);
											$date       = get_the_date('d.m.Y', $match->ID);
											$time       = get_the_date('H:i', $match->ID);
											$time       = ($time == '13:13') ? '' : '<br>o ' . $time;
											$link       = get_permalink($match->ID);
										?>
										<tr>
											<td class="t-right"><?php echo $date . $time; ?></td>
											<td><?php echo $home[0] . ' <strong>-</strong> ' . $guest[0]; ?></td>
											<td><?php echo ((strlen($homeScore[0]) == 0) ? 0 : $homeScore[0]) . ' : ' . ((strlen($guestScore[0]) == 0) ? 0 : $guestScore[0]); ?></td>
											<td><a href="<?php echo $link; ?>">detail&nbsp;zápasu</a></td>
										</tr>
										<?php endforeach; ?>
									</table>
									<br>
								<?php endforeach; ?>
							<?php endif; ?>
							<?php echo apply_filters('the_content', $post->post_content); ?>
						</div>
					</div>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png"><br>
					<div class="section-column">
						<?php
							$args = array();
							$args['type'] = 'zapas';
							echo WP_FullCalendar::calendar($args);
						?>
					</div>
				</div></div>
			</div>
		</div></div>
<?php get_footer(); ?>