<?php get_header(); ?>
<div id="subtitle">
	<div class="wrapper">
		<h2>Kategória: <?php single_cat_title(); ?></h2>
	</div>
</div>
<div id="content">
	<div class="wrapper">
		<div id="side">
			<?php
				$args = array('orderby' => 'name', 'order' => 'ASC');
				$categories = get_categories($args);
				foreach($categories as $category) {
					if (isset($br)) { echo $br; } else { $br = '<br />'; }
					echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
					echo '<p> Description:'. $category->description . '</p>';
					echo '<p> Post Count: '. $category->count . '</p>';
				}
			?>
			</div>
		<div id="main">
			<?php
				$paged = get_query_var('paged') ? get_query_var('paged') : 1;
				$cat_id = get_cat_ID( single_cat_title(null, false) );
				query_posts( "cat=$cat_id&paged=$paged&posts_per_page=10" );
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						if (isset($br)) { echo $br; } else { $br = '<br />'; }
						echo '<article class="article">';
						echo '<header class="header">';
							the_title('<a href="'.get_permalink().'"><h2>', '</h2></a>');
						echo '</header>';
						echo '<div class="content">';
						echo '<small>'.the_time('j.F Y').'</small>';
						echo '</div>';
							pdf24Plugin_link('Stiahni Pdf');
						echo '</article>';
					endwhile;
				else :
			?>
				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'posam' ); ?></h1>
					</header>

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'posam' ); ?></p>
						<?php get_search_form(); ?>
					</div>
				</article>

			<?php endif; ?>
			</div>
<?php get_footer(); ?>
