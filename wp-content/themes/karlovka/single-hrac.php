<?php get_header(); ?>
	<div id="main" class="section"><div class="wrapper">
		<div class="section-row">
			<div class="t-third"><div class="wrapper">
				<div id="content">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php
							$ID = get_the_ID();
							$tim = get_post_meta($ID, 'timy_post_class', true);

							$number  = get_post_custom_values('wpcf-number', $ID);
							$name    = get_post_custom_values('wpcf-name', $ID);
							$surname = get_post_custom_values('wpcf-surname', $ID);
							$fullname = $name[0] . ' ' . $surname[0];

							$nick    = get_post_custom_values('wpcf-nick', $ID);
							$yearly  = get_post_custom_values('wpcf-yearly', $ID);
							$post    = get_post_custom_values('wpcf-post', $ID);
							$state   = get_post_custom_values('wpcf-state', $ID);
							$school  = get_post_custom_values('wpcf-school', $ID);
							if (!empty($date[0])) {
								$birth    = date('d.m.Y', $date[0]);
							}
							$birth    = date('d.m.Y', $date[0]);

							$height  = get_post_custom_values('wpcf-height', $ID);
							$weight  = get_post_custom_values('wpcf-weight', $ID);

							$img     = get_the_post_thumbnail($ID);
						?>
						<h1>#<?php echo $number[0] . ' ' .  get_the_title($ID); ?></h1>
						<div class="content-wrapper">
							<div class="half">
								<?php echo (empty($img)) ? '<img src="' . get_template_directory_uri() . '/img/img-player.png" alt="' . $fullname . '" title="' . $fullname . '">' : $img; ?>
							</div>
							<div class="half">
								<table class="player">
									<tr>
										<td>
											<strong>Meno a priezvisko:</strong><br>
											<?php echo $fullname; ?>
										</td>
									</tr>
									<?php if (!empty($nick[0])) : ?>
									<tr>
										<td>
											<strong>Prezývka:</strong> "<?php echo $nick[0]; ?>"
										</td>
									</tr>
									<?php endif; ?>
									<tr>
										<td>
											<strong>Post:</strong> <?php echo strtoupper($post[0]); ?>
										</td>
									</tr>
									<tr>
										<td>
											<strong>Stav:</strong> <?php echo strtoupper($state[0]); ?>
										</td>
									</tr>
<!-- 									<tr> -->
<!-- 										<td> -->
<!-- 											<strong>Dátum narodenia:</strong><br> -->
											<?php //echo $birth; ?>
<!-- 										</td> -->
<!-- 									</tr> -->
									<tr>
										<td>
											<strong>Ročník:</strong> <?php echo $yearly[0]; ?>
										</td>
									</tr>
									<tr>
										<td>
											<strong>Výška:</strong> <?php echo $height[0]; ?>
										</td>
									</tr>
									<tr>
										<td>
											<strong>Váha:</strong> <?php echo $weight[0]; ?>
										</td>
									</tr>
									<?php if (!empty($school[0])) : ?>
									<tr>
										<td>
											<strong>Škola:</strong> <?php echo $school[0]; ?>
										</td>
									</tr>
									<?php endif; ?>
								</table>
							</div>
							<div class="full">
								<br>
								<?php echo apply_filters('the_content', get_the_content()); ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div></div>
			<div class="third banner"><div class="wrapper">
				<?php echo do_shortcode('[swiper tim="'.$tim[0].'"]');?>
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
			</div></div>
		</div>
	</div></div>
<?php get_footer(); ?>