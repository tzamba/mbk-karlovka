					<div class="article-single">
						<h1>
							<?php the_title(); ?>
							<br>
							<small class="date"><?php echo get_the_date('d. F Y'); ?></small><br>
							<div class="fb-box">
								<div class="fb-like" data-href="<?php echo get_permalink(); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
							</div>
						</h1>
						<div class="content-wrapper">
							<?php $thumb = get_the_post_thumbnail(get_the_ID(), 'medium'); ?>
							<?php if (!empty($thumb)) : ?>
								<div class="article-image">
									<?php echo get_the_post_thumbnail($id, 'medium'); ?>
								</div>
							<?php endif; ?>
							<?php echo apply_filters('the_content', $post->post_content); ?>
						</div>
					</div>