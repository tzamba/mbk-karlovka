<?php get_header(); ?>
	<div id="main" class="section"><div class="wrapper">
		<div class="section-row">
			<div class="t-third"><div class="wrapper">
				<div id="content">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php
							$ID = get_the_ID();
							$phone    = get_post_custom_values('wpcf-phone', $ID);
							$email    = get_post_custom_values('wpcf-email', $ID);
							$function = get_post_custom_values('wpcf-function', $ID);

							$name     = get_post_custom_values('wpcf-name', $ID);
							$surname  = get_post_custom_values('wpcf-surname', $ID);
							$fullname = $name[0] . ' ' . $surname[0];

							$date    = get_post_custom_values('wpcf-date', $ID);
							if (!empty($date[0])) {
								$birth    = date('d.m.Y', $date[0]);
							}

							$img     = get_the_post_thumbnail($ID);
						?>
						<h1><?php echo get_the_title($ID); ?></h1>
						<div class="content-wrapper">
							<div class="half">
								<?php echo (empty($img)) ? '<img src="' . get_template_directory_uri() . '/img/img-player.png" alt="' . $fullname . '" title="' . $fullname . '">' : $img; ?>
							</div>
							<div class="half">
								<table class="player">
									<tr>
										<td>
											<strong>Meno a priezvisko:</strong><br>
											<h2><?php echo $fullname; ?></h2>
										</td>
									</tr>
									<?php if (isset($birth)) : ?>
									<tr>
										<td>
											<strong>Dátum narodenia:</strong><br>
											<?php echo $birth; ?>
										</td>
									</tr>
									<?php endif; ?>
									<tr>
										<td>
											<strong>Telefón:</strong><br>
											<?php echo $phone[0]; ?>
										</td>
									</tr>
									<tr>
										<td>
											<strong>Email:</strong><br>
											<?php echo $email[0]; ?>
										</td>
									</tr>
									<tr>
										<td>
											<strong>Funkcie:</strong><br>
											<?php echo $function[0]; ?>
										</td>
									</tr>
								</table>
							</div>
							<div class="full">
								<?php echo apply_filters('the_content', get_the_content()); ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div></div>
			<div class="third banner"><div class="wrapper">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
			</div></div>
		</div>
	</div></div>
<?php get_footer(); ?>