<?php
/*
Template Name: Frontpage
*/
	get_header();
?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/sk_SK/sdk.js#xfbml=1&appId=264030927116984&version=v2.0";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
		<div id="main" class="section"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<?php getFrontpageNews(); ?>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<?php getBanner(BAN_MAIN, true); ?>
					<!--
						<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
					 -->
				</div></div>
			</div>

			<div class="section-row">
				<div class="third"><div class="wrapper">
					<h2>Zápasy</h2>
					<div class="section-column section-column-nopadding">
						<?php getLastMatches(); ?>
					</div>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Kalendár zápasov</h2>
					<div class="section-column">
						<div class="nameday">
							<p><?php echo Meniny(); ?></p>
						</div>
						<div id="calendar">
							<?php
								$args = array();
								$args['type'] = 'zapas';
								echo WP_FullCalendar::calendar($args);
							?>
						</div>
					</div>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Správy</h2>
					<div class="section-column section-column-nopadding">
						<?php getMiniNews(); ?>
					</div>
				</div></div>
			</div>

			<div class="section-row">
				<div class="third"><div class="wrapper">
					<h2>Galéria</h2>
					<div class="section-column gallery-small">
						<?php echo do_shortcode('[recent max=9]'); ?>
						<a href="<?php echo home_url('/'); ?>media/fotogaleria" class="more b-blue-light t-smaller"><i class="fa fa-angle-double-right"></i> viac fotiek vo fotogalérii</a>
					</div>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Facebook</h2>
					<div class="section-column">
						<div class="fb-like-box" data-href="https://www.facebook.com/mbkkarlovka" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
					</div>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Youtube</h2>
					<div class="section-column">
						<iframe src="//www.youtube.com/embed/iFUGNsXk9RU?rel=0" width="280" height="210" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						<a href="<?php echo home_url('/'); ?>media/videogaleria" class="more b-blue-light t-smaller"><i class="fa fa-angle-double-right"></i> viac videí vo videogalérii</a>
					</div>
				</div></div>
			</div>

			<div class="section-row">
				<div class="third"><div class="wrapper">
<!-- 					<h2>PODUJATIE</h2> -->
					<?php getBanner(BAN_FRON_01); ?>
				</div></div>
				<div class="third"><div class="wrapper">
<!-- 					<h2>PODUJATIE</h2> -->
					<?php getBanner(BAN_FRON_02); ?>
				</div></div>
				<div class="third"><div class="wrapper">
<!-- 					<h2>FAN SHOP</h2> -->
					<?php getBanner(BAN_FRON_03); ?>
				</div></div>
			</div>

<!--
			<div class="section-row">
				<div class="third"><div class="wrapper">
 					<h2>PODUJATIE</h2>
					<?php getBanner(BAN_FRON_04); ?>
				</div></div>
				<div class="third"><div class="wrapper">
 					<h2>PODUJATIE</h2>
					<?php getBanner(BAN_FRON_05); ?>
				</div></div>
				<div class="third"><div class="wrapper">
 					<h2>PODUJATIE</h2>
					<?php getBanner(BAN_FRON_06); ?>
				</div></div>
			</div>
-->

<!--
			<div class="section-row">
				<div class="full banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-02.png">
				</div></div>
			</div>
-->
		</div></div>

		<div id="partners" class="section"><div class="wrapper">
			<img src="<?php echo get_template_directory_uri(); ?>/img/vsemvs.png" alt="VŠEMvs" title="VŠEMvs">
			<img src="<?php echo get_template_directory_uri(); ?>/img/slsp.png" alt="Nadácia SLSP" title="Nadácia SLSP">
			<img src="<?php echo get_template_directory_uri(); ?>/img/basketland.png" alt="Basketland" title="Basketland">
			<img src="<?php echo get_template_directory_uri(); ?>/img/rrgroup.png" alt="R+R Group" title="R+R Group">
		</div></div>
<?php get_footer(); ?>