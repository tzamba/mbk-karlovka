<?php get_header(); ?>
	<div id="main" class="section"><div class="wrapper">
		<div class="section-row">
			<div class="t-third"><div class="wrapper">
				<div id="content">
					<h1><?php the_title(); ?></h1>
					<div class="content-wrapper">
							<table class="bordered">
							<?php
								$home       = get_post_custom_values('wpcf-home', $post->ID);
								$guest      = get_post_custom_values('wpcf-guest', $post->ID);
								$homeScore  = get_post_custom_values('wpcf-home-score', $post->ID);
								$guestScore = get_post_custom_values('wpcf-guest-score', $post->ID);
								$date       = get_the_date('d.m.Y', $post->ID);
								$time       = get_the_date('H:i', $post->ID);
								$time       = ($time == '13:13') ? '' : '<br>o ' . $time;
							?>
								<tr>
									<td class="t-right">Dátum podujatia:<br><?php echo $date . $time; ?></td>
									<td class="t-right t-bigger"><?php echo ((strlen($homeScore[0]) == 0) ? 0 : $homeScore[0]) . '<br>' . $home[0]; ?></td>
									<td class="t-bigger"><?php echo ((strlen($guestScore[0]) == 0) ? 0 : $guestScore[0]). '<br>' . $guest[0]; ?></td>
								</tr>
							</table>
						<?php echo apply_filters('the_content', $post->post_content); ?>
					</div>
				</div>
			</div></div>
			<div class="third banner"><div class="wrapper">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png"><br>
				<div class="section-column">
					<?php
						$args = array();
						$args['type'] = 'zapas';
						echo WP_FullCalendar::calendar($args);
					?>
				</div>
			</div></div>
		</div>
	</div></div>
<?php get_footer(); ?>