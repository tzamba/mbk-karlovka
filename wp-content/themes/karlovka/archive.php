<nav id="nav-single">
	<h3 class="assistive-text"><?php _e( 'Post navigation' , 'posam'); ?></h3>
	<span class="nav-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous', 'posam' ) ); ?></span>
	<span class="nav-next"><?php next_post_link( '%link', __( 'Next <span class="meta-nav">&rarr;</span>', 'posam' ) ); ?></span>
</nav>
