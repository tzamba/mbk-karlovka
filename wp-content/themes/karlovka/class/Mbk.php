<?php

class Mbk {

	const MEMBER_CATEGORY_PLAYER  = 'Hráč';
	const MEMBER_CATEGORY_TRAINER = 'Tréner';
	const MEMBER_CATEGORY_REAL    = 'Realizačný tím';
	const MEMBER_CATEGORY_PARENT  = 'Rodič';
	const MEMBER_CATEGORY_FAN     = 'Fanúšik';

	const MEMBER_TYPE_YOUNG = 'Mládež <18';
	const MEMBER_TYPE_ADULT = 'Dospelí >18';
	const MEMBER_TYPE_VIP   = 'VIP';

	public $memberCategories = array(
		self::MEMBER_CATEGORY_PLAYER =>  '1',
		self::MEMBER_CATEGORY_TRAINER => '2',
		self::MEMBER_CATEGORY_REAL =>    '3',
		self::MEMBER_CATEGORY_PARENT =>  '4',
		self::MEMBER_CATEGORY_FAN =>     '5'
	);

	public $memberType = array(
		self::MEMBER_TYPE_YOUNG => '1',
		self::MEMBER_TYPE_ADULT => '2',
		self::MEMBER_TYPE_VIP =>   '3'
	);

	public function getCategoryNumber($cat) {
		return $this->memberCategories[$cat];
	}

	public function getTypeNumber($type) {
		return $this->memberType[$type];
	}

	public function getVariableSymbol($id) {
		$season = $this->getSeason('y', '');

		$category = get_post_meta($id, 'wpcf-clen-category', true);
		$category = $this->getCategoryNumber($category);

		$type = get_post_meta($id, 'wpcf-clen-type', true);
		$type = $this->getTypeNumber($type);

		return $season . $category . $type . $id;
	}

	public function getSeason($format = 'Y', $delimiter = '-') {
		$thisYear = date('Y');
		$startSeason = date($format, strtotime('01-06-'.$thisYear));
		$endSeason = date($format, strtotime('30-05-'.($thisYear+1)));
		$season = $startSeason . $delimiter . $endSeason;
		return $season;
	}

	public function getMembershipPrice($type) {
		$price = 12;
		switch ($type){
			case 'Mládež <18':
				$price = 12;
				break;
			case 'Dospelí >18':
				$price = 24;
				break;
			case 'VIP':
				$price = 120;
				break;
		}

		return $price;
	}

	public function getPaymentTable($memberId) {
		$variableSymbol = $this->getVariableSymbol($memberId);
		
		$type     = get_post_meta($memberId, 'wpcf-clen-type', true);
		$surname  = get_post_meta($memberId, 'wpcf-clen-surname', true);
		$category = get_post_meta($memberId, 'wpcf-clen-category', true);

		$price = $this->getMembershipPrice($type);
		
		return '
			<table style="backgroud-color: #e3eaf5; padding: 10px;">
				<tr>
					<td><strong>Suma:</strong></td>
					<td>'.$price.' EUR</td>
				</tr>
				<tr>
					<td><strong>Na číslo účtu:</strong></td>
					<td>Tatra Banka: 2626012133 / 1100</td>
				</tr>
				<tr>
					<td><strong>Variabilný symbol:</strong></td>
					<td>'.$variableSymbol.'</td>
				</tr>
				<tr>
					<td><strong>Špecifický symbol:</strong></td>
					<td>prvých 6 čísel rodného čísla</td>
				</tr>
				<tr>
					<td style="vertical-align: top;"><strong>Správa pre príjemcu:</strong></td>
					<td>
						'.$surname.' '.$category.' '.$this->getSeason().'
					</td>
				</tr>
			</table>';
	}
}