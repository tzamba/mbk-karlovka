<?php
require 'php/meniny.php';
require 'class/Mbk.php';

// CATEGORIES
define('CAT_NOVINKA',    3);
define('CAT_BLESKOVKA',  4);
define('CAT_SPOD_KOSOV', 5);
define('CAT_NAPISALI',   6);

// POST TYPES
define('PTYPE_POST',      'post');
define('PTYPE_PAGE',      'page');
define('PTYPE_CLEN',      'clen');
define('PTYPE_ZAPAS',     'zapas');
define('PTYPE_PODUJATIE', 'podujatie');
define('PTYPE_HRAC',      'hrac');
define('PTYPE_TRENER',    'trener');
define('PTYPE_TIM',       'tim');
define('PTYPE_BANNER',    'banner');
define('PTYPE_SEASON',    'season');

// TAXONOMIES
define('TAX_SUTAZ',     'sutaz');
define('TAX_KATEGORIA', 'kategoria');

// BANNERS
define('BAN_MAIN',    '4610');
define('BAN_FRON_01', '4615');
define('BAN_FRON_02', '4616');
define('BAN_FRON_03', '4617');
define('BAN_FRON_04', '15568');
define('BAN_FRON_05', '15569');
define('BAN_FRON_06', '15570');

add_action('after_setup_theme', 'mbk_after_setup_theme');
if (!function_exists('mbk_after_setup_theme')) {
	function mbk_after_setup_theme() {
		load_theme_textdomain('mbk', get_template_directory() . '/languages');

		mbk_clean_header();
		mbk_remove_texturize();
		mbk_register_nav_menus();
		mbk_register_sidebars();
		mbk_clean_markup();
		mbk_load_assets();
		add_theme_support('post-thumbnails');
	}
}

if (!function_exists('mbk_load_assets')) {
	function mbk_load_assets() {
		$version = 2.26;
		if (!is_admin()) {
			//wp_deregister_script('l10n');
			//wp_deregister_script('jquery');

			wp_register_style('posam-style', get_stylesheet_uri(), array(), $version, 'all');
			wp_enqueue_style('posam-style');
		}
	}
}
add_action('wp_enqueue_scripts', 'mbk_load_assets');

if (!function_exists('mbk_clean_header')) {
	function mbk_clean_header() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'index_rel_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'feed_links_extra', 3);
// 		remove_action('wp_head', 'wp_print_styles', 8, 0);
		remove_action('wp_head', 'start_post_rel_link', 10, 0);
		remove_action('wp_head', 'parent_post_rel_link', 10, 0);
		remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
	}
}

if (!function_exists('mbk_remove_texturize')) {
	function mbk_remove_texturize() {
		remove_filter('category_description', 'wptexturize');
		remove_filter('list_cats', 'wptexturize');
		remove_filter('comment_author', 'wptexturize');
		remove_filter('comment_text', 'wptexturize');
		remove_filter('single_post_title', 'wptexturize');
		remove_filter('the_title', 'wptexturize');
		remove_filter('the_content', 'wptexturize');
		remove_filter('the_excerpt', 'wptexturize');
	}
}

if (!function_exists('mbk_register_nav_menus')) {
	function mbk_register_nav_menus() {
		register_nav_menus(
			array(
				'header-menu' => __('Header menu', 'mbk'),
				'footer-menu 1' => __('Footer menu 1', 'mbk'),
				'footer-menu 2' => __('Footer menu 2', 'mbk')
			)
		);
	}
}

if (!function_exists('mbk_register_sidebars')) {
	function mbk_register_sidebars() {
		if (function_exists('register_sidebar')) {
			register_sidebar(
				array(
					'id' => 'sidebar',
					'name' => __('Sidebar', 'mbk'),
					'before_widget' => '<aside id="%1$s" class="aside %2$s">',
					'after_widget' => '</aside>',
					'before_title' => '<header class="header"><h1 class="title">',
					'after_title' => '</h1></header>'
				)
			);
		}
	}
}

if (!function_exists('mbk_clean_markup')) {
	function mbk_clean_markup() {
		add_filter('wp_list_categories', 'mbk_clean_category_list_rel');
		add_filter('the_category', 'mbk_clean_category_list_rel');
		// odstrani automaticke pridavenie <p> tagov
		// remove_filter('the_content', 'wpautop');
// 		if (!defined('WP_ADMIN') && defined('WP_USE_THEMES')) {
// 			ob_start('mbk_clean_markup_start');
// 			add_action('shutdown', 'mbk_clean_markup_flush');
// 		}
	}
}

if (!function_exists('mbk_clean_category_list_rel')) {
	function mbk_clean_category_list_rel($text) {
		$text = str_replace('rel="category tag"', 'rel="tag"', $text);
		$text = str_replace('rel="category"', 'rel="tag"', $text);
		return $text;
	}
}

if (!function_exists('mbk_clean_markup_start')) {
	function mbk_clean_markup_start($buffer) {
		$buffer = mbk_clean_quotes($buffer);
		$buffer = mbk_clean_slash($buffer);
		return $buffer;
	}
}

if (!function_exists('mbk_clean_quotes')) {
	function mbk_clean_quotes($buffer) {
		preg_match_all("~<[a-z]+[^<>]*='[^<>]*>~i", $buffer, $matched_tags);

		foreach ($matched_tags[0] as $tag) {
			unset($converted_tag);
			preg_match_all("~\s([a-z]+)='(.*?)'~", $tag, $matched_attributes);

			foreach ($matched_attributes[0] as $key => $attributes_string) {
				if (isset($matched_attributes[1][$key], $matched_attributes[2][$key])) {
					if (!isset($converted_tag)) $converted_tag = $tag;
					$converted_tag = str_replace(trim($attributes_string), $matched_attributes[1][$key].'="'.$matched_attributes[2][$key].'"', $converted_tag);
				}
			}

			if (isset($converted_tag)) $buffer = str_replace($tag, $converted_tag, $buffer);
		}

		return $buffer;
	}
}

if (!function_exists('mbk_clean_slash')) {
	function mbk_clean_slash($buffer) {
		return preg_replace('/(<.+)\s\/>/', '$1>', $buffer);
	}
}

// if (!function_exists('mbk_clean_markup_flush')) {
// 	function mbk_clean_markup_flush() {
// 	    ob_end_flush();
// 	}
// }

function mbk_wp_title($title, $sep) {
	if (is_feed()) {
		return $title;
	}
	global $page, $paged;

	$title .= get_bloginfo('name', 'display');

	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) {
		$title .= " $sep $site_description";
	}

	if (($paged >= 2 || $page >= 2) && !is_404()) {
		$title .= " $sep " . sprintf(__('Page %s', '_s'), max($paged, $page));
	}

	return $title;
}
add_filter('wp_title', 'mbk_wp_title', 10, 2);


add_filter('request', 'mbk_request_filter');
function mbk_request_filter($query_vars) {
	if(isset($_GET['s']) && empty($_GET['s'])) {
		$query_vars['s'] = " ";
	}
	return $query_vars;
}

add_filter('the_content', 'remove_empty_p', 20, 1);
function remove_empty_p($content){
	// clean up p tags around block elements
	$content = preg_replace(array(
			'#<p>\s*<(div|aside|section|article|header|footer)#',
			'#</(div|aside|section|article|header|footer)>\s*</p>#',
			'#</(div|aside|section|article|header|footer)>\s*<br ?/?>#',
			'#<(div|aside|section|article|header|footer)(.*?)>\s*</p>#',
			'#<p>\s*</(div|aside|section|article|header|footer)#',
	), array(
			'<$1',
			'</$1>',
			'</$1>',
			'<$1$2>',
			'</$1',
	), $content);

	return preg_replace('#<p>(\s|&nbsp;)*+(<br\s*/*>)*(\s|&nbsp;)*</p>#i', '', $content);
}

/********************/
/* CUSTOM FUNCTIONS */
/********************/
function show_future_posts($posts)
{
	global $wp_query, $wpdb;
	if (is_single() && $wp_query->post_count == 0) {
		$posts = $wpdb->get_results($wp_query->request);
	}
	return $posts;
}
add_filter('the_posts', 'show_future_posts');

function custom_excerpt_length($length) {
	return 15;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

function mbk_excerpt_more($more) {
	global $post;
	return ' <a href="'. get_permalink($post->ID) . '" class="show-more">...viac</a>';

}
add_filter('excerpt_more', 'mbk_excerpt_more');

function get_file_extension($file) {
	return strtolower(substr(strrchr($file,'.'),1));
}

function getRandomBanner() {
	$banner = "";

	return $banner;
}

function getFrontpageNews() {
	$topNews = '<div id="top-news">';
	$topNewSlider = '<div id="top-news-slider">';
	$topNewStrip = '<div id="top-news-strips">';

	$query = new WP_Query(array('post_type' => PTYPE_POST, 'category__in' => array(CAT_NOVINKA), 'posts_per_page' => 5, 'orderby' => 'date'));
	while ($query->have_posts()) :
		$query->the_post();
		$topNewSlider .= '
			<div class="top-news-slide">
				' . get_the_post_thumbnail($query->post->ID, 'medium') . '
				<div class="title-wrapper">
					<a href="' . get_permalink() . '" class="title">
						<h3>' . get_the_title() . '</h3>
						<span>' . get_the_excerpt() . '</span>
					</a>
				</div>
			</div>';
		$topNewStrip .= '
			<a href="#" class="top-news-strip">
				<table>
					<tr>
						<td>' . get_the_post_thumbnail($query->post->ID, 'thumbnail') . '</td>
						<td><span>' . get_the_title() . '</span></td>
					</tr>
				</table>
			</a>';
	endwhile;
	$topNewSlider .= '</div>';
	$topNewStrip .= '</div>';
	$topNews .= $topNewSlider . $topNewStrip . '</div>';

	wp_reset_query();
	echo $topNews;
}

function getMiniNews() {
// 	$today = getdate();
// 	$queryArchive = new WP_Query(array(
// 		'post_type' => PTYPE_POST,
// 		'posts_per_page' => 5,
// 		'orderby' => 'date',
// 		'order' => 'DESC',
// 		'date_query' => array(
// 				array(
// 					'before' => array(
// 						'year'  => $today['year'],
// 						'month' => $today['mon'],
// 						'day'   => $today['mday']
// 					)
// 				)
// 			)
// 		)
// 	);
	$querySpodKosov = new WP_Query(array('post_type' => PTYPE_POST, 'category__in' => array(CAT_SPOD_KOSOV), 'posts_per_page' => 15, 'orderby' => 'date'));
	$queryNapisali  = new WP_Query(array('post_type' => PTYPE_POST, 'category__in' => array(CAT_NAPISALI), 'posts_per_page' => 15, 'orderby' => 'date'));

	echo '
		<div class="tabs">
			<div class="half tab-header active" data-tab="#news-second"><div class="wrapper">
				<a href="#">Napísali o nás</a>
			</div></div>
			<div class="half tab-header" data-tab="#news-first"><div class="wrapper">
				<a href="#">Spod košov</a>
			</div></div>
			<div id="news-first" class="tab-item"><div class="wrapper">
				<div class="mscroll">
					<table class="bordered">
						<tbody>';
				while ($querySpodKosov->have_posts()) :
					$querySpodKosov->the_post();
					echo '<tr><td><a href="'.get_permalink().'">' .get_the_date('d.m.Y'). '<br><strong>' . get_the_title() . '</strong></a></td></tr>';
				endwhile;
				echo '<tr><td><a href="'.home_url('/').'aktuality/spod-kosov/" class="more"><i class="fa fa-angle-double-right"></i> viac článkov</a></td></tr>';
		echo '
						</tbody>
					</table>
				</div>
			</div></div>
			<div id="news-second" class="tab-item"><div class="wrapper">
				<div class="mscroll">
					<table class="bordered">
						<tbody>';
				while ($queryNapisali->have_posts()) :
					$queryNapisali->the_post();
					echo '<tr><td><a href="'.get_permalink().'">' .get_the_date('d.m.Y'). '<br><strong>' . get_the_title() . '</strong></a></td></tr>';
				endwhile;
				echo '<tr><td><a href="'.home_url('/').'aktuality/napisali-o-nas/" class="more"><i class="fa fa-angle-double-right"></i> viac článkov</a></td></tr>';
		echo '
						</tbody>
					</table>
				</div>
			</div></div>
		</div>';
}

function getLastMatches() {
	$currentDate = getdate();
	$future = getMatchesByCategory(null, 15, $currentDate[0], 'ASC');
	$last   = getMatchesByCategory(null, 15, $currentDate[0], 'DESC');

	echo '
		<div class="tabs">
			<div class="half tab-header active" data-tab="#event-first"><div class="wrapper">
				<a href="#">Najbližšie</a>
			</div></div>
			<div class="half tab-header" data-tab="#event-second"><div class="wrapper">
				<a href="#">Posledné</a>
			</div></div>
			<div id="event-first" class="tab-item"><div class="wrapper">
				<div class="mscroll">
					<table class="bordered">
						<tbody>';
		foreach ($future as $post) {
			$home  = get_post_custom_values('wpcf-home', $post->ID);
			$guest = get_post_custom_values('wpcf-guest', $post->ID);
			$link  = get_permalink($post->ID);
			$time  = get_the_date('H:i', $post->ID);
			$time = ($time == '13:13') ? '' : ' o ' . $time;
			echo '<tr><td><a href="'.$link.'">' . get_the_date('d.m.Y', $post->ID) . $time . '<br><strong>' . $home[0] . ' - ' . $guest[0] . '</strong></a></td></tr>';
		}
		echo '<tr><td><a href="'.home_url('/').'/aktuality/zapasy" class="more"><i class="fa fa-angle-double-right"></i> všetky zápasy</a></td></tr>';
		echo '
						</tbody>
					</table>
				</div>
			</div></div>
			<div id="event-second" class="tab-item"><div class="wrapper">
				<div class="mscroll">
					<table class="bordered">
						<tbody>';
		foreach ($last as $post) {
			$home  = get_post_custom_values('wpcf-home', $post->ID);
			$guest = get_post_custom_values('wpcf-guest', $post->ID);
			$homeScore  = get_post_custom_values('wpcf-home-score', $post->ID);
			$guestScore = get_post_custom_values('wpcf-guest-score', $post->ID);
			$link  = get_permalink($post->ID);
			echo '<tr><td><a href="'.$link.'">' . get_the_date('d.m.Y', $post->ID) . '<br><strong>' . $home[0] . ' ' . $homeScore[0] . ':' . $guestScore[0] . ' ' . $guest[0] . '</strong></a></td></tr>';
		}
		echo '<tr><td><a href="'.home_url('/').'/aktuality/zapasy" class="more"><i class="fa fa-angle-double-right"></i> všetky zápasy</a></td></tr>';
		echo '
						</tbody>
					</table>
				</div>
			</div></div>
		</div>';
}

function getMatchesByCategory($team = null, $limit = -1, $date = null, $order = 'DESC', $dateFrom = '', $dateTo = '') {
	$args = array(
			'post_type' => PTYPE_ZAPAS,
			'post_status' => 'any',
			'order'    => $order,
			'orderby' => 'date',
			'posts_per_page' => $limit
		);
	if (!is_null($date)) {
		$when = ('ASC' == $order) ? 'after' : 'before';
		$day  = ('ASC' == $order) ? '-1 days' : 'now';
		$today = getdate(date('U',strtotime($day)));
		$args['date_query'] = array(
			array(
				$when => array(
					'year'  => $today['year'],
					'month' => $today['mon'],
					'day'   => $today['mday']
				)
			)
		);
	}
	if (strtotime($dateFrom) || strtotime($dateTo)) {
		$args['date_query'] = array();
		if (strtotime($dateFrom)) {
			$today = getdate(date('U', strtotime('-1 day', strtotime($dateFrom))));
			$dateFrom = array(
					'after'  => array(
						'year'  => $today['year'],
						'month' => $today['mon'],
						'day'   => $today['mday']
					)
				);
			array_push($args['date_query'], $dateFrom);
		}
		if (strtotime($dateTo)) {
			$today = getdate(date('U', strtotime('+1 day', strtotime($dateTo))));
			$dateTo = array(
					'before'  => array(
						'year'  => $today['year'],
						'month' => $today['mon'],
						'day'   => $today['mday']
					)
				);
			array_push($args['date_query'], $dateTo);
		}
	}

	if (!is_null($team)) {
		$args['meta_query'] = array(
			array(
				'key' => 'timy_post_class',
				'value' => '"' . $team . '"' ,
				'compare' => 'LIKE'
			)
		);
	}

	$season = get_query_var('season');
	if (empty($season)) {
		$date = date('Y-m-d');
		$thisYear = date('Y');
		$startSeason = date('Y-m-d', strtotime('01-06-'.$thisYear));
		$endSeason = date('Y-m-d', strtotime('30-05-'.($thisYear+1)));

		$season = ($thisYear-1).'-'.$thisYear;
		if ($startSeason < $date && $date < $endSeason) {
			$season = $thisYear.'-'.($thisYear+1);
		}
	}

//	$args['meta_query'] = array(
//		array(
//			'key' => 'season_post_class',
//			'value' => '"' . $season . '"' ,
//			'compare' => 'LIKE'
//		)
//	);

	$query = new WP_Query($args);
//	echo "Last SQL-Query: {$query->request}";
	$matches = array();
	while ($query->have_posts()) :
		$query->the_post();
		$post = $query->post;
		$matches[] = $post;
	endwhile;

	wp_reset_query();
	return $matches;
}

function getPlayersByCategory($tax = null) {
	$args = array(
			'post_type' => PTYPE_HRAC,
			'post_status' => 'any',
			'order'    => 'DESC',
			'orderby' => 'meta_value_num',
			'meta_key' => 'wpcf-yearly',
			'posts_per_page' => -1
		);
	if (!is_null($tax)) {
		$args['meta_query'] = array(
				array(
					'key' => 'timy_post_class',
					'value' => '"' . $tax . '"' ,
					'compare' => 'LIKE'
				)
			);
	}

	$query = new WP_Query($args);
	$matches = array();
	while ($query->have_posts()) :
		$query->the_post();
		$post = $query->post;
		$matches[] = $post;
	endwhile;

	wp_reset_query();
	return $matches;
}

function getBanner($id, $hideHeader = false) {
	$args = array(
			'post_type' => PTYPE_BANNER,
			'post_status' => 'any',
			'p' => $id
		);

	$query = new WP_Query($args);
	while ($query->have_posts()) :
		$query->the_post();
		$post = $query->post;
		echo ($hideHeader) ? '' :  '<h2>' . $post->post_title . '</h2>';
		echo apply_filters('the_content', $post->post_content);
	endwhile;
}

function get_right_sidebar() {
	echo '<div class="aside">';
	$right = types_render_field("right-sidebar", array());
	if (isset($right) && strlen($right) > 0) {
		echo $right;
	}

	$referencie = get_vybrane_referencie();
	if (strlen($referencie) > 0) {
		echo '<h3 class="title">'.__('Selected references', 'mbk').'</h3>';
		echo $referencie;
	}

	$temy  = '';
	$temy .= get_suvisiace_temy(__('Top themes', 'mbk') , PTYPE_TOP_TEMY);
	$temy .= get_suvisiace_temy(__('News', 'mbk'), PTYPE_POST);
	$temy .= get_suvisiace_temy(__('Conferences', 'mbk') , PTYPE_KONFERENCIE);
	if (strlen($temy) > 0) {
		echo '<p></p><hr><h3 class="title">'.__('Related links', 'mbk').'</h3>';
		echo $temy;
	}
}

function get_mbk_lat() {
	$lat = types_render_field("latitude", array());
	echo (isset($lat) && strlen($lat) > 0) ? $lat : '48.177653';
}

function display_attachements($content) {
	global $post;

	if ($post->post_type == 'page' && $post->post_status == 'publish') {
		$attachments = get_posts(array(
				'post_type' => 'attachment',
				'post_parent' => $post->ID
		));

		// CI CHCEME NA DANEJ STRANKE ZOBRAZIT PRILOHY
		$displayAttachements = types_render_field("attachements", array());
		if ((isset($displayAttachements) && $displayAttachements == 'yes') && $attachments) {
			$content .= '<article><p>';
			$content .= '<div class="attachments"><div>';
			foreach ($attachments as $attachment) {
				$title = wp_get_attachment_link($attachment->ID, false);
				$img = wp_get_attachment_image($attachment->ID, 'thumbnail', true);
				$content .= $img . ' ' .$title;
				$content .= (isset($br)) ? $br : $br = '<br><br>';
			}
			$content .= '<div></div>';
			$content .= '</p></article>';
		}
	}

	return $content;
}

function filesize2human ($num) {
	$filesize_tmp1=$num/1024;
	if ($filesize_tmp1<1024): $filesize=Round($filesize_tmp1,2).' KB';
	else: $filesize=Round(($filesize_tmp1/1024),2).' MB';
	endif;
	return str_replace(".",",",$filesize);
}

/* NINJA FORMS */
define('FORM_MEMBERSHIP',         1);
define('FORM_PROLONG_MEMBERSHIP', 2);

define('FIELD_NAME',      1);
define('FIELD_SURNAME',   6);
define('FIELD_DATE',      7);
define('FIELD_EMAIL',     2);
define('FIELD_ADDRESS',   8);
define('FIELD_JOB',       9);
define('FIELD_CATEGORY',  10);
define('FIELD_TYPE',      11);
define('FIELD_TSHIRT',    12);
define('FIELD_MEMBER_ID', 13);

define('FIELD_PROLONG_NAME',    15);
define('FIELD_PROLONG_SURNAME', 16);
define('FIELD_PROLONG_EMAIL',   17);

function ninja_forms_register_example(){
	add_action('ninja_forms_pre_process', 'mbk_form_pre_process');
	add_action('ninja_forms_display_pre_init', 'mbk_form_pre_init');
}
add_action('init', 'ninja_forms_register_example');

function mbk_form_pre_process() {

}

function mbk_form_pre_init($form_id) {
	global $ninja_forms_loading;
	if (is_null($ninja_forms_loading)) return;

// $sett = $ninja_forms_loading->get_all_form_settings();
// var_dump($sett);

	if ($form_id == FORM_PROLONG_MEMBERSHIP) {
		$name = get_query_var('cName');
		$surname = get_query_var('cSurname');
		$ninja_forms_loading->update_field_value(FIELD_PROLONG_NAME, $name);
		$ninja_forms_loading->update_field_value(FIELD_PROLONG_SURNAME, $surname);
	}
}

function add_change_ninja_forms_admin_email(){
	add_action('ninja_forms_post_process', 'change_ninja_forms_admin_email');
}
add_action('init', 'add_change_ninja_forms_admin_email');

function change_ninja_forms_admin_email() {
	global $ninja_forms_processing;

	$meta = array(
		FIELD_NAME     => 'wpcf-clen-name',
		FIELD_SURNAME  => 'wpcf-clen-surname',
		FIELD_DATE     => 'wpcf-clen-date',
		FIELD_EMAIL    => 'wpcf-clen-email',
		FIELD_ADDRESS  => 'wpcf-clen-address',
		FIELD_JOB      => 'wpcf-clen-job',
		FIELD_CATEGORY => 'wpcf-clen-category',
		FIELD_TYPE     => 'wpcf-clen-type',
		FIELD_TSHIRT   => 'wpcf-clen-tshirt'
	);

	$form_id = $ninja_forms_processing->get_form_ID();
	if ($form_id == FORM_MEMBERSHIP) {
		$allFields = $ninja_forms_processing->get_all_fields();
		if(is_array($allFields)) {
			// insert new clen
			$fullname = $allFields[FIELD_NAME] . ' ' . $allFields[FIELD_SURNAME];
			$newClen = array(
				'post_title'    => $fullname,
				'post_type'     => PTYPE_CLEN,
				'post_content'  => '',
				'post_status'   => 'publish',
				'post_author'   => 1
			);
			$memberID = wp_insert_post($newClen);

			foreach($allFields as $fieldId => $userValue) {
				// add clen meta
				$key   = $meta[$fieldId];
				$value = $userValue;
				if (!is_null($key)) {
					if (!update_post_meta ($memberID, $key, $value)) {
						add_post_meta($memberID, $key, $value, true);
					};
				}
			}
		}

		$dept     = $ninja_forms_processing->get_field_value(FIELD_CATEGORY);
		$type     = $ninja_forms_processing->get_field_value(FIELD_TYPE);
		$shirt    = $ninja_forms_processing->get_field_value(FIELD_TSHIRT);

		$mbk = new Mbk();
		$paymentTable = $mbk->getPaymentTable($memberID);

		$price = $mbk->getMembershipPrice($type);
		$variableSymbol = $mbk->getVariableSymbol($memberID);
		
		$email = '
<table>
	<tr>
		<td>
			Dobrý deň,<br><br>
			ďakujeme za zaslanie prihlášky za člena v klube MBK Karlovka Bratislava.<br><br>
			Vaša prihláška - „'.$memberID.' – '.$dept.'“ bola prijatá, označili ste konfekčnú veľkosť „'.$shirt.'“ bolo Vám pridelené číslo klubovej karty <strong>'.$memberID.'</strong><br><br>
			Prosíme o uhradenie členského poplatku: <strong>'.$price.' EUR</strong>
		</td>
		<td>
			<a href="http://www.mbkkarlovka.sk"><img src="http://www.mbkkarlovka.sk/wp-content/themes/karlovka/img/logo.png" title="MBK Karlovka" alt="MBK Karlovka"></a>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			'.$paymentTable.'
		</td>
	</tr>
	<tr>
		<td colspan="2">
			Po uhradení členského, prijatí a spárovaní platby Vám bude zaslaný akceptačný mail, na základe ktorého Vám do 7 pracovných dní vyrobíme klubovú kartu, pripravíme objednané klubové tričko, ktoré si následne môžete vyzdvihnúť na najbližšom domácom zápase.<br><br>
			Prajeme Vám pekný deň.<br><br>
			<strong>Ing. Martina Mackovičová</strong> – sekretariát MBK Karlovka<br>
			<strong>Mail:</strong> martina.mackovicova@gmail.com<br>
			<strong>Mob:</strong> +421 902 853 465<br>
			<strong>Web:</strong> www.mbkkarlovka.sk
		</td>
	</tr>
</table>
		';
		$ninja_forms_processing->update_form_setting('user_email_msg', $email);

		// SEND ADMIN INFO
		$admin  = $ninja_forms_processing->get_form_setting('admin_email_msg');
		$admin .= $paymentTable;
		$ninja_forms_processing->update_form_setting('admin_email_msg', $admin);




					$ninja_forms_processing->update_form_setting('admin_mailto', array('testzmziznu@zombi.sk'));





	} else if ($form_id == FORM_PROLONG_MEMBERSHIP) {
		$name    = $ninja_forms_processing->get_field_value(FIELD_PROLONG_NAME);
		$surname = $ninja_forms_processing->get_field_value(FIELD_PROLONG_SURNAME);
		$email   = $ninja_forms_processing->get_field_value(FIELD_PROLONG_EMAIL);

		if (!empty($name) && !empty($surname)) {
			global $wpdb;
			$title = $name . ' ' . $surname;
			$sql = "SELECT * FROM z_posts WHERE post_type = '".PTYPE_CLEN."' AND post_title LIKE '%".$title."%'";
			$posts = $wpdb->get_results($sql);

			if (count($posts) > 0) {
				$memberID = $posts[0]->ID;
				$savedEmail = get_post_meta($memberID, 'wpcf-clen-email', true);
				if ($savedEmail == $email) {
					$ninja_forms_processing->update_form_setting('success_msg', 'Ďakujeme, na Váš e-mail sme Vám zaslali informácie o zaplatení členského. Hneď ako budeme platbu evidovať členské Vám bude predĺžené.');

					$mbk = new Mbk();
					$paymentTable = $mbk->getPaymentTable($memberID);

					$adminSubject  = $ninja_forms_processing->get_form_setting('admin_subject');
					$ninja_forms_processing->update_form_setting('admin_subject', $adminSubject . ' - ' . $title);

					$adminMsg = 'Žiadosť o predĺženie členstva pre používateľa: ' . $title . ',<br><br>' . $paymentTable;
					$ninja_forms_processing->update_form_setting('admin_email_msg', $adminMsg);

					$ninja_forms_processing->update_form_setting('user_email_msg', 'Dobrý deň,<br><br>sme veľmi radi, že ste sa rozhodli predĺžiť si členstvo v klube MBK Karlovka. Jediné čo zostáva spraviť je zaplatiť členské s nasledujúcimi údajmi:' . $paymentTable . '<br><br>Pekný deň praje,<br><br>Tím MBK Karlovka');
				} else {
					$ninja_forms_processing->update_form_setting('success_msg', 'E-mail, ktorý ste zadali nie je priradený k členovi: <br><br><strong>' . $title . '</strong><br><br> Zadajte správny e-mail, alebo <a href="/o-klube/klubove-clenstvo/prihlaska-do-kluboveho-clenstva/">vyplňte prihlásku</a>.');
					$ninja_forms_processing->update_form_setting('user_subject', 'Mbkkarlovka - Obnova členstva');
					$ninja_forms_processing->update_form_setting('user_email_msg', 'Dobrý deň,<br><br>zo stránky www.mbkkarlovka.sk sme zaznamenali pokus o obnovu členstva pre používateľa <strong>' . $title . '</strong>, ktorý zadal email ' . $email . '. Daný email avšak nie je spárovaný s používateľom. Ak ste to Vy, prosím zaregistrujte sa nanovo, prípadne požiadajte administrátora aby priradil tento email k Vašemu členstvu.<br><br>Pekný deň praje,<br><br>Tím MBK Karlovka');

					$ninja_forms_processing->update_form_setting('admin_mailto', array());
				}
			} else {
				$ninja_forms_processing->update_form_setting('success_msg', 'Je nám ľúto ale člena:<br><br><strong>' . $title . '</strong><br><br>v našej databáze neevidujeme. Pozrite si, či sa nachádzate v <a href="/o-klube/clenovia-klubu/">zozname členov</a> alebo <a href="/o-klube/klubove-clenstvo/prihlaska-do-kluboveho-clenstva/">vyplňte prihlásku</a>.');
				$ninja_forms_processing->update_form_setting('user_email_msg', 'Dobrý deň,<br><br>zo stránky www.mbkkarlovka.sk sme zaznamenali pokus o obnovu členstva pre používateľa <strong>' . $title . '</strong>, ktorý zadal email ' . $email . '. Bohužiaľ takého člena v našej databáze neevidujeme, ak sa chcete stať členom MBK Karlovka prosím <a href="http://www.mbkkarlovka.sk/o-klube/klubove-clenstvo/prihlaska-do-kluboveho-clenstva/">vyplňte prihlásku</a>.<br><br>Pekný deň praje,<br><br>Tím MBK Karlovka');

				$ninja_forms_processing->update_form_setting('admin_mailto', array());
			}
		}
	}
}


/* CUSTOM META BOXES */
// Reference Meta Box //
add_action('load-post.php', 'timypost_meta_boxes_setup');
add_action('load-post-new.php', 'timypost_meta_boxes_setup');

function timypost_meta_boxes_setup() {
	add_action('add_meta_boxes', 'mbk_add_post_meta_boxes');
	add_action('save_post', 'timysave_post_class_meta', 10, 2);
	add_action('save_post', 'seasonsave_post_class_meta', 10, 2);
}

function mbk_add_post_meta_boxes() {
	$types = array(
			PTYPE_HRAC,
			PTYPE_ZAPAS,
			PTYPE_TRENER,
			PTYPE_PAGE
		);
	foreach ($types as $type) {
		add_meta_box(
				'timy_post_class',
				__('Tímy', 'posam'),			// Title
				'timy_post_class_meta_box',		// Callback function
				$type,					// Admin page (or post type)
				'side',							// Context
				'default'						// Priority
		);
	}
	$types = array(
			PTYPE_TIM,
			PTYPE_ZAPAS
		);
	foreach ($types as $type) {
		add_meta_box(
				'season_post_class',
				__('Seasons', 'posam'),			// Title
				'season_post_class_meta_box',		// Callback function
				$type,					// Admin page (or post type)
				'side',							// Context
				'default'						// Priority
		);
	}
}

function timy_post_class_meta_box($object, $box) {
	wp_nonce_field(basename(__FILE__), 'timy_post_class_nonce');
	echo '<p>';
	// echo '<label>'.__('Choose references you want to add to the page.', 'posam').'</label>';
	// echo '<br />';
	$references = get_post_meta($object->ID, 'timy_post_class', true);
	$wp_ref = new WP_Query(array('post_type' => PTYPE_TIM, 'posts_per_page' => -1, 'order' => 'ASC'));
	echo '<table>';
	while ($wp_ref->have_posts()) :
	$ref_post = $wp_ref->next_post();
	$value = $ref_post->ID;
	echo '<tr><td title="'.$value.'">';
	echo '<input id="'.$value.'" type="checkbox" ';
	if (is_array($references) && in_array($value, $references)) {
		echo 'checked="checked"';
	}
	echo 'name="timy_post_class[]" value="'.$value.'" /> <label for="'.$value.'">'.$ref_post->post_title.'</lable>';
	echo' </tr></td>';
	endwhile;
	echo '</table>';
	echo '</p>';
}

function timysave_post_class_meta($post_id, $post) {
	if (!isset($_POST['timy_post_class_nonce']) || !wp_verify_nonce($_POST['timy_post_class_nonce'], basename(__FILE__)))
		return $post_id;
	$post_type = get_post_type_object($post->post_type);
	if (!current_user_can($post_type->cap->edit_post, $post_id))
		return $post_id;

	$new_meta_value = (isset($_POST['timy_post_class']) ? $_POST['timy_post_class'] : '');
	$meta_key = 'timy_post_class';
	$meta_value = get_post_meta($post_id, $meta_key, true);

	if ($new_meta_value && '' == $meta_value) {
		add_post_meta($post_id, $meta_key, $new_meta_value, true);
	} elseif ($new_meta_value && $new_meta_value != $meta_value) {
		update_post_meta($post_id, $meta_key, $new_meta_value);
	} elseif ('' == $new_meta_value && $meta_value) {
		delete_post_meta($post_id, $meta_key, $meta_value);
	}
}

function season_post_class_meta_box($object, $box) {
	wp_nonce_field(basename(__FILE__), 'season_post_class_nonce');
	echo '<p>';
	// echo '<label>'.__('Choose references you want to add to the page.', 'posam').'</label>';
	// echo '<br />';
	$references = get_post_meta($object->ID, 'season_post_class', true);
	$wp_ref = new WP_Query(array('post_type' => PTYPE_SEASON, 'posts_per_page' => -1, 'order' => 'ASC'));
	echo '<table>';
	while ($wp_ref->have_posts()) :
	$ref_post = $wp_ref->next_post();
	$value = $ref_post->post_title;
	echo '<tr><td title="'.$value.'">';
	echo '<input id="'.$value.'" type="checkbox" ';
	if (is_array($references) && in_array($value, $references)) {
		echo 'checked="checked"';
	}
	echo 'name="season_post_class[]" value="'.$value.'" /> <label for="'.$value.'">'.$ref_post->post_title.'</lable>';
	echo' </tr></td>';
	endwhile;
	echo '</table>';
	echo '</p>';
}

function seasonsave_post_class_meta($post_id, $post) {
	if (!isset($_POST['season_post_class_nonce']) || !wp_verify_nonce($_POST['season_post_class_nonce'], basename(__FILE__)))
		return $post_id;
	$post_type = get_post_type_object($post->post_type);
	if (!current_user_can($post_type->cap->edit_post, $post_id))
		return $post_id;

	$new_meta_value = (isset($_POST['season_post_class']) ? $_POST['season_post_class'] : '');
	$meta_key = 'season_post_class';
	$meta_value = get_post_meta($post_id, $meta_key, true);

	if ($new_meta_value && '' == $meta_value) {
		add_post_meta($post_id, $meta_key, $new_meta_value, true);
	} elseif ($new_meta_value && $new_meta_value != $meta_value) {
		update_post_meta($post_id, $meta_key, $new_meta_value);
	} elseif ('' == $new_meta_value && $meta_value) {
		delete_post_meta($post_id, $meta_key, $meta_value);
	}
}
// Reference Meta Box //

// Shortcodes //
// [boxes][/boxes]
function shortBoxes($atts, $content = null){
	return "<div class='boxes'>" . strip_tags(do_shortcode($content), "<a>") . "</div>";
}
add_shortcode('boxes', 'shortBoxes');

// [box anchor="section-01"]Text linku[/box]
function shortBox($atts, $content = null){
	$param = shortcode_atts( array(
			'anchor' => 'undefined',
			'link'   => 'undefined'
	), $atts);

	$link = ($param['anchor'] == 'undefined') ? $param['link'] : '#' . $param['anchor'];
	return "<a href='" . $link . "' class='box'>" . $content . "</a>";
}
add_shortcode('box', 'shortBox');

// [anchor anchor="section-01"]
function shortAnchor($atts, $content = null){
	$param = shortcode_atts( array(
			'anchor' => 'section'
	), $atts);
	return "<div id='" . $param['anchor'] . "' class='anchor'></div>";
}
add_shortcode('anchor', 'shortAnchor');

// [hraci tim="351"]
function shortHraci($atts){
	$param = shortcode_atts( array(
			'tim' => 'default'
	), $atts);

	$players = getPlayersByCategory($param['tim']);
	$postR = array();
	$postK = array();
	$postP = array();
	foreach ($players as $player) {
		$post    = get_post_custom_values('wpcf-post', $player->ID);
		switch ($post[0]) {
			case 'r':
				$postR[] = $player;
				break;
			case 'k':
				$postK[] = $player;
				break;
			default:
				$postP[] = $player;
				break;
		}
	}
	$posts = array($postR, $postK, $postP);
	$result = "";

	if (!empty($players)) {
		$result .= '<table class="bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Meno</th>
					<th>Ročník</th>
					<th>Post</th>
					<th>Stav</th>
					<th>Škola</th>
					<th></th>
				</tr>
			</thead>';
		foreach ($posts as $players) {
			foreach ($players as $player) {
				$number  = get_post_custom_values('wpcf-number', $player->ID);
				$name    = get_post_custom_values('wpcf-name', $player->ID);
				$surname = get_post_custom_values('wpcf-surname', $player->ID);
				$yearly  = get_post_custom_values('wpcf-yearly', $player->ID);
				$post    = get_post_custom_values('wpcf-post', $player->ID);
				$state   = get_post_custom_values('wpcf-state', $player->ID);
				$school  = get_post_custom_values('wpcf-school', $player->ID);
				$link    = get_permalink($player->ID);
				$result .= '<tr>';
					$result .= '<td>' . $number[0] . '</td>';
					$result .= '<td>' . $name[0] . '&nbsp;' . $surname[0] . '</td>';
					$result .= '<td>' . $yearly[0] . '</td>';
					$result .= '<td>' . strtoupper($post[0]) . '</td>';
					$result .= '<td>' . strtoupper($state[0]) . '</td>';
					$result .= '<td>' . ((!empty($school)) ? $school[0] : '') . '</td>';
					$result .= '<td><a href="' . $link . '">detail&nbsp;hráča</a></td>';
				$result .= '</tr>';
			}
		}
		$result .= '</table>';
	}

	return $result;
}
add_shortcode('hraci', 'shortHraci');

// [clenovia]
function shortClenovia($atts) {
	$result = '';
	$query  = new WP_Query(array(
		'post_type' => PTYPE_CLEN,
		'posts_per_page' => -1,
		'order'    => 'ASC',
		'orderby' => 'meta_value',
		'meta_key' => 'wpcf-clen-surname',
	));

	// ALWAYS LAST 3 YEARS
	$yearCount = 3;

	$result .= '
	<table class="bordered members">
	<thead class="header">
		<tr>
			<td></td>
			<td>Meno</td>
			<td>Kategória</td>
			<td colspan="3">
				<table>
					<tr>';
		for ($i = $yearCount - 2; $i >= 0; $i--) {
			$result .= '<td class="t-center">'.(date('Y')-$i).'-'.(date('Y')-$i+1).'</td>';
		}
		$result .= '</tr>
				</table>
			</td>
		</tr>
	</thead>
	<tbody>';
	while ($query->have_posts()) {
		$query->the_post();
		$post = $query->post;
		$name     = get_post_custom_values('wpcf-clen-name', $post->ID);
		$surname  = get_post_custom_values('wpcf-clen-surname', $post->ID);
		$category = get_post_custom_values('wpcf-clen-category', $post->ID);
		$paid     = get_post_custom_values('wpcf-clen-paid', $post->ID);
		$season   = get_post_custom_values('wpcf-clen-season', $post->ID);
		$seasons  = array_slice(process_checkboxes($season), -3, 3, true);
		$result .= '<tr><td class="td-min"><i class="fa fa-user"></i></td>';
		$result .= '<td class="td-min"><a href="/o-klube/klubove-clenstvo/predlzenie-kluboveho-clenstva?cName='.$name[0].'&cSurname='.$surname[0].'"><strong>'.$name[0].' '.$surname[0].'</strong></a></td>';
		$result .= '<td>' . $category[0] . '</td>';

		for ($i = 0; $i < $yearCount - 1; $i++) {
			$result .= '<td class="t-center"><i class="fa ' . ((!empty($seasons[$i])) ? 'fa-check-circle t-green' : 'fa-times-circle t-red')  . '"></i></td>';
		}
	};
	$result .= '</table><br>';
	$result .= '<div class="t-right">Celkový počet členov: <strong>' . $query->post_count . '</strong></div>';

	return $result;
}
add_shortcode('clenovia', 'shortClenovia');

// [zapasy tim="360" filter="false" class="class"]
function shortZapasy($atts) {
	$param = shortcode_atts( array(
		'tim' => 'undefined',
		'class' => '',
		'filter' => true
	), $atts);
	$result= "";

	$param['filter'] = ($param['filter'] === 'false') ? false : true;

	if (!empty($_POST)) {
		$from = !empty($_POST['date-from']) ? $_POST['date-from'] : '';
		$to   = !empty($_POST['date-to']) ? $_POST['date-to'] : '';
		$matches  = getMatchesByCategory($param['tim'], -1, null, 'ASC', $from, $to);
	} else {
		$d = get_query_var('d');
		if (!empty($d)) {
			$currentDate = getdate();
			if ($d == 'next') {
				$matches = getMatchesByCategory($param['tim'], -1, $currentDate[0], 'ASC');
			} else if ($d == 'prev') {
				$matches = getMatchesByCategory($param['tim'], -1, $currentDate[0], 'DESC');
			}
		} else {
			$matches  = getMatchesByCategory($param['tim']);
		}
	}

	$categories = array();
	foreach ($matches as $match) {
		$taxId = get_post_meta($match->ID, 'timy_post_class', true);
		$team   = get_post($taxId[0]);

		$cat = $team->post_title;
		if (!array_key_exists($cat, $categories)) {
			$categories[$cat] = array();
		}
		$categories[$cat][] = $match;
	}

	if ($param['filter']) {
		$inputForm = '
			<form action="" method="POST" class="form-filter">
				<label>Dátum zápasu od: <input type="text" name="date-from" value="'.$from.'" class="date-from"></label>
				<label>do: <input type="text" name="date-to" value="'.$to.'" class="date-to"></label>
				<input type="submit" value="Filter" onClick=\'ga("send", "event", "Filter", "click", "Filter match - '.$param['tim'].'", 1);\'>
			</form>
		';
	}

	if (!empty($categories)) {
		foreach ($categories as $name => $category) {
			$rand = mt_rand();
			$result .= '<h2 class="t-center '.$param['class'].'" data-collapse-header="'.$rand.'">Tím: ' . $name . '</h2>';

			$result .= $inputForm;

			$result .= '<div data-collapse-body="'.$rand.'" class="'.$param['class'].'"><table class="bordered">
				<thead>
					<tr>
						<th>Dátum</th>
						<th>Zápasy</th>
						<th>Výsledok</th>
						<th></th>
					</tr>
				</thead>';
				foreach ($category as $match) {
					$home       = get_post_custom_values('wpcf-home', $match->ID);
					$guest      = get_post_custom_values('wpcf-guest', $match->ID);
					$homeScore  = get_post_custom_values('wpcf-home-score', $match->ID);
					$guestScore = get_post_custom_values('wpcf-guest-score', $match->ID);
					$date       = get_the_date('d.m.Y', $match->ID);
					$time       = get_the_date('H:i', $match->ID);
					$time       = ($time == '13:13') ? '' : '<br>o ' . $time;
					$link       = get_permalink($match->ID);
				$result .= '<tr>
					<td class="t-right">' . $date . $time . '</td>
					<td>' . $home[0] . ' <strong>-</strong> ' . $guest[0] . '</td>
					<td>' . ((strlen($homeScore[0]) == 0) ? 0 : $homeScore[0]) . ' : ' . ((strlen($guestScore[0]) == 0) ? 0 : $guestScore[0]) . '</td>
					<td><a href="' . $link . '">detail&nbsp;zápasu</a></td>
				</tr>';
				};
			$result .= '</table></div>
			<br>';
		}
	} else {
		$result .= $inputForm;
//		$result .= '<div><p>Pre zvolené kritériá sa nenašli žiadne zápasy.</p></div>';
	}

	return $result;
}
add_shortcode('zapasy', 'shortZapasy');

// [swiper]
function shortSwiper($atts) {
	$param = shortcode_atts( array(
		'tim' => '351'
	), $atts);

	$players = getPlayersByCategory($param['tim']);

	$result = '';

	if (empty($players)) {
		return $result;
	}

//	$result . "" . var_dump($players);

	$result .= '<div class="swiper-container swiper-players" data-autoplay="5000" data-pagination=".swiper-pagination">';
	$result .= '<div class="swiper-arrows">';
	$result .= '<a href="#" class="previous"><i class="fa fa-chevron-left"></i></a>';
	$result .= '<a href="#" class="next"><i class="fa fa-chevron-right"></i></a></div>';
	$result .= '<div class="swiper-pagination"></div>';
	$result .= '<div class="swiper-wrapper">';


	foreach ($players as $player) {
		$result .= '<div class="swiper-slide">';

		$number  = get_post_custom_values('wpcf-number', $player->ID);
		$name    = get_post_custom_values('wpcf-name', $player->ID);
		$surname = get_post_custom_values('wpcf-surname', $player->ID);
		$fullname = $name[0] . ' ' . $surname[0];

		$yearly  = get_post_custom_values('wpcf-yearly', $player->ID);
		$post    = get_post_custom_values('wpcf-post', $player->ID);
		$state   = get_post_custom_values('wpcf-state', $player->ID);
		$school  = get_post_custom_values('wpcf-school', $player->ID);
		$link    = get_permalink($player->ID);
		$img     = get_the_post_thumbnail($player->ID	);

		$result .= (empty($img)) ? '<img class="img-player" src="' . get_template_directory_uri() . '/img/img-player.png" alt="' . $fullname . '" title="' . $fullname . '">' : $img;

		$result .= '<div class="player-info boxes"><div>';
		$result .= '<strong>Číslo:</strong> ' . $number[0];
		$result .= '<br><strong>Meno</strong>: ' . $fullname;
		$result .= '<br><strong>Ročník</strong>: ' . $yearly[0];
		$result .= '</div><a class="box" href="' . $link . '" onClick=\'ga("send", "event", "Profile", "click", "Profile - '.$fullname.'", 1);\'>Celý profil</a>';
		$result .= '</div>';

		$result .= '</div>';
	}

	$result .= '</div>';
	$result .= '</div>';
	$result .= '<br>';

	return $result;
}
add_shortcode('swiper', 'shortSwiper');

/* QUERY PARAMS */
function add_query_vars_filter($vars) {
	$vars[] = "member-accept";
	$vars[] = "season";
	$vars[] = "d";
	$vars[] = "cName";
	$vars[] = "cSurname";
	return $vars;
}
add_filter('query_vars', 'add_query_vars_filter');

function process_checkboxes($checkboxes) {
	if (empty($checkboxes)) return array();

	$values = array();
	$arr = unserialize($checkboxes[0]);
	foreach ($arr as $val) {
		array_push($values, $val[0]);
	}

	return $values;
}