<?php get_header(); ?>
		<?php $cssClass = get_post_meta( get_the_id(), 'css_class', true); ?>
		<div id="main" class="section <?php echo $cssClass; ?>"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<div id="content">
						<?php
							// display_breadcrumbs();
							get_template_part('content');
						?>
					</div>
				</div></div>
				<div class="third banner"><div class="wrapper">
				<?php
					$dir     = get_template_directory_uri();
					$banner  = $dir . '/img/banner-01.png';
					$file    = 'wp-content/themes/karlovka/img/banner-' . $cssClass . '.png';
					if (file_exists($file)) {
						$banner = $dir . '/img/banner-' . $cssClass . '.png';
					}
				?>
					<img src="<?php echo $banner; ?>">
				</div></div>
			</div>
		</div></div>
<?php get_footer(); ?>