jQuery.noConflict();

;(function($) {
	var methods = {
		init: function(options) {
		    var settings = $.extend({
				pause: true,
				arrows: ['', ''],
				bullets: false,
				interval: 6000,
				slides: '.feature'
		    }, options);
		
			return this.each(function(options) {
				var $this = $(this), data = $this.data('slide');
				
				if (data !== undefined) return;

				slides = $this.find(settings.slides), size = slides.size(), current = 0;
				
				data = {
					settings: settings,
					slides: slides,
					size: size,
					current: current
				}

				$this
					.data('slide', data)
					.trigger('init.slide')
					.slide('decorate')
					.slide('move', 0)
					.slide('resume')
					.hover(
						function() { $(this).addClass('hover').slide('pause'); },
						function() { $(this).removeClass('hover').slide('resume'); }
					);
			});
		},
		
		move: function(index) {
			return this.each(function() {	
				var $this = $(this), data = $this.data('slide');

				data.current = index;
				slides = data.slides;

				if (data.bullets !== undefined) data.bullets
					.find('a')
					.eq(index).addClass('active')
					.siblings().removeClass('active');
					
				slides
					.removeClass('previous active next')
					.eq(index).addClass('active');

				slides.eq((index === 0) ? data.size - 1 : index - 1).addClass('previous');
				slides.eq((index === data.size - 1) ? 0 : index + 1).addClass('next');

				$this
					.toggleClass('first', index === 0)
					.toggleClass('last', index === data.size - 1)
					.data('slide', data)
					.trigger('move.slide');
			});
		},
		
		next: function() {
			return this.each(function() {	
				var $this = $(this), data = $this.data('slide');

				index = data.current + 1;
				if (index === data.size) index = 0;

				$this
					.trigger('next.slide')
					.slide('move', index);
			});
		},
		
		previous: function() {
			return this.each(function() {	
				var $this = $(this), data = $this.data('slide');
				
				index = data.current - 1;
				if (index < 0) index = data.size - 1;
				
				$this
					.trigger('previous.slide')
					.slide('move', index);
			});
		},
		
		resume: function() {
			return this.each(function() {	
				var $this = $(this), data = $this.data('slide');

				if (data.interval === undefined || data.settings.pause) {
					clearInterval(data.interval);
					data.interval = setInterval(function() { $this.slide('next'); }, data.settings.interval);
				}
			});
		},
		
		pause: function() {
			return this.each(function() {	
				var $this = $(this), data = $this.data('slide');
				
				if (data.settings.pause) clearInterval(data.interval);
			});
		},

		decorate: function() {
			return this.each(function() {
				var $this = $(this), data = $this.data('slide'), controls = $('<div class="controls"></div>');
					
				if (data.settings.arrows !== false) {
					arrows = data.settings.arrows;
					data.arrows = $('<div class="arrows"><a class="previous" href="#">' + arrows[0] + '</a><a class="next" href="#">' + arrows[1] + '</a></div>').appendTo(controls);
				}
				
				if (data.settings.bullets !== false) {
					bullets = '';
					for (i = 1; i < size + 1; i++) bullets += '<a class="bullet" href="#">' + data.settings.bullets.replace('$', i) + '</a>';
					
					data.bullets = $('<div class="bullets">' + bullets + '</div>').appendTo(controls);
				}
				
				$this
					.append(controls)
					.data('slide', data)
					.find('.controls a')
					.click(function($event) {
						var $element = $(this), $this = $element.parents('.controls').parent(), data = $this.data('slide');
	
						$event.preventDefault();
						
						if ($element.hasClass('bullet')) $this.slide('move', data.bullets.find('a').index($element));
						if ($element.hasClass('previous')) $this.slide('previous');
						if ($element.hasClass('next')) $this.slide('next');
					});
			});
		},
		
		destroy: function() {
			return this.each(function() {
				var $this = $(this), data = $this.data('slide');
					
				$this
					.trigger('destroy.slide')
					.removeData('slide')
					.removeClass('first last hover')
					.off('.slide');
				
				data.slides.removeClass('previous next active')
				data.arrows.remove();
				data.bullets.remove();
			});
		}
	};
		
	$.fn.slide = function(method) {
		if (methods[method]) return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		else if (typeof method === 'object' || !method) return methods.init.apply(this, arguments);
	};
})(jQuery);

;(function($) {
	$(function() {
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) input.val('').removeClass('placeholder');
			if (input.data('original')) input.attr('type', input.data('original')).data('original', false);
		}).blur(function() {
  			var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) input.addClass('placeholder').val(input.attr('placeholder'));
			if (input.attr('type') == 'password') input.attr('type', 'text').data('original', 'password');
		}).blur().parents('form').submit(function() {
			$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) input.val('');
			});
		});
	});
})(jQuery);

;(function($) {
	;addComment={moveForm:function(d,f,i,c){var m=this,a,h=m.I(d),b=m.I(i),l=m.I("cancel-comment-reply-link"),j=m.I("comment_parent"),k=m.I("comment_post_ID");if(!h||!b||!l||!j){return}m.respondId=i;c=c||false;if(!m.I("wp-temp-form-div")){a=document.createElement("div");a.id="wp-temp-form-div";a.style.display="none";b.parentNode.insertBefore(a,b)}h.parentNode.insertBefore(b,h.nextSibling);if(k&&c){k.value=c}j.value=f;l.style.display="";l.onclick=function(){var n=addComment,e=n.I("wp-temp-form-div"),o=n.I(n.respondId);if(!e||!o){return}n.I("comment_parent").value="0";e.parentNode.insertBefore(o,e);e.parentNode.removeChild(e);this.style.display="none";this.onclick=null;return false};try{m.I("comment").focus()}catch(g){}return false},I:function(a){return document.getElementById(a)}};

	$('#feature').slide({
		pause: true,
		arrows: false,
		bullets: '',
		interval: 7000,
		slides: '.feature'
	});
	
//	$(".collapse").collapse('hide');
	
	
	var sideheight = 0;
	$('#side .cases .slides .slide').each(function () {
		sideheight = Math.max($(this).height(), sideheight);
	});
	
	$('#main > .article').each(function() {
		if ((aside = $(this).find('.aside')).size()) {
			content = $(this).find('.content');
			
			content.css('min-height', Math.max(aside.height(), content.height()));
		}
	});
	
	interval = ($('body').hasClass('home')) ? 7000 : 4500;
	
	$('#side .cases .slides')
		.height(sideheight + 40)
		.slide({
			pause: true,
			arrows: ['Prev', 'Next'],
			bullets: '',
			interval: interval,
			slides: '.slide'
		});
		
	$('.listing2 .articles.closed .title a').on('click', function (ev) {
		articles = $(this).parents('.articles')
		
		if (articles.hasClass('closed')) {
			ev.preventDefault();
			articles.removeClass('closed');
		}
	});
	
	$('#primarymenu').detach().appendTo('body');

	$('#side .cases .bullets').width($('#side .cases .bullets .bullet').size() * 19);
	
	$('#contactmap').each(function() {
		var lat = $(this).attr('data-lat'),
			lon = $(this).attr('data-lon'),
			zoom = parseInt($(this).attr('data-zoom'), 10),
			latlng = new google.maps.LatLng(lat, lon),
	    	options = {
				scrollwheel: false,
				navigationControl: false,
				mapTypeControl: false,
				scaleControl: false,
				streetViewControl: false,
				zoom: zoom,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				backgroundColor: "#cccccc"
			},
			map = new google.maps.Map(document.getElementById('contactmap'), options),
			icon = new google.maps.MarkerImage('http://posam.odyzeoclients.com/wp-content/themes/posam/img/pin.png', new google.maps.Size(210, 115), new google.maps.Point(0, 0), new google.maps.Point(10, 110)),
			marker = new google.maps.Marker({ position: latlng, map: map, icon: icon });

		google.maps.event.addListener(marker, 'click', function() { 
			window.open('https://maps.google.com/maps?q=' + lat + ',' + lon + '&z=' + zoom, '_blank');
		});

		google.maps.event.addListener(map, 'click', function() { 
			window.open('https://maps.google.com/maps?q=' + lat + ',' + lon + '&z=' + zoom, '_blank');
		});
	});

})(jQuery);
