<?php get_header(); ?>
<div id="subtitle">
	<div class="wrapper">
		<h2><?php get_subtitle(); ?></h2>
	</div>
</div>
<div id="content">
	<div class="wrapper">
		<div id="side">
			<?php get_sidebar(); ?>
		</div>
		<div id="main">
			<?php display_breadcrumbs(); ?>
			<?php
				$content = types_render_field("content-from-page", array());
				if (isset($content) && strlen($content) > 0) {
					$id = $post->ID;
					if ($post = get_page(intval($content))) {
// 						get content from another page
					} else {
						$post = get_page($id);
					}
					get_template_part('content');
				} else {
					if (have_posts()) {
						while (have_posts()) {
							the_post();
							get_template_part('content');
						}
					} else {
						get_template_part('content', 'nothing');
					}
				}
			?>
		</div>
<?php get_footer(); ?>