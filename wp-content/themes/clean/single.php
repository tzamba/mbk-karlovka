<?php get_header(); ?>
<div id="subtitle">
	<div class="wrapper">
		<h2>Thinking</h2>
	</div>
</div>
<div id="content">
	<div class="wrapper">
		<div id="side">
			<?php
				wp_nav_menu(array(
						'theme_location' => 'thinking',
						'container' => 'nav',
						'container_class' => 'menu',
						'menu_class' => 'top-menu',
						'echo' => true,
						'fallback_cb' => '',
						'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>'
				));
			?>
			<?php
// 				$args = array('orderby' => 'name', 'order' => 'ASC');
// 				$categories = get_categories($args);
// 				foreach($categories as $category) {
// 					if (isset($br)) { echo $br; } else { $br = '<br />'; }
// 					echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
// 					echo '<p> Description:'. $category->description . '</p>';
// 					echo '<p> Post Count: '. $category->count . '</p>';
// 				}
			?>
			</div>
		<div id="main">
			<?php $category = get_the_category(); ?>
			<nav class="breadcrumbs">
				<a title="Ísť na Domov." href="/" class="site-home">Domov</a><span> &rarr; </span>
				<a title="Ísť na Thinking" href="/thinking/" class="site-home">Thinking</a><span> &rarr; </span>
				<a title="Ísť na <?php echo $category[0]->name; ?>." href="/thinking/<?php echo $category[0]->slug; ?>" class="page"><?php echo $category[0]->name; ?></a><span> &rarr; </span><?php the_title(); ?>
			</nav>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="article-<?php the_ID(); ?>" class="article article-wide">
					<header class="header">
						<h1 class="title"><?php the_title(); ?></h1>
						<?php get_extended_title(); ?>
					</header>
					<div class="content">
						<?php echo wpautop(apply_filters('the_content', $post->post_content)); ?>
						<p><a class="top" href="#header">späť na vrch</a></p>
						<div class="meta"><?php edit_post_link(__('Edit', 'posam'), ' '); ?></div>
					</div>
				</article>
			<?php endwhile; ?>
		</div>
<?php get_footer(); ?>