		<div id="footer" class="section"><div class="wrapper">
			<div id="copy">MBK Karlovka &copy; <?php echo date('Y'); ?> | <a href="/">www.mbkkarlovka.sk</a></div>

			<div class="third"><div class="wrapper">
				<ul>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna dlsha ako ostatne</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
				</ul>
			</div></div>
			<div class="third"><div class="wrapper">
				<ul>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
					<li><a href="#">Menu polozka jedna dlsha ako ostatne</a></li>
					<li><a href="#">Menu polozka jedna</a></li>
				</ul>
			</div></div>
			<div class="third t-white"><div class="wrapper">
				<strong>Kontaktné informácie:</strong>
				<p>
					<br>
					MBK Karlovka<br>
					Sladkovicova 7, 831 04<br>
					Bratislava<br>
					<br>
					<i class="fa fa-phone-square"></i> 0910 404 007<br>
					<i class="fa fa-envelope"></i> <a href="mailto:info@mbkkalovka.sk">info@mbkkalovka.sk</a>
				</p>
			</div></div>
		</div></div>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script>;window.jQuery || document.write('<script src="lib/jquery/jquery-1.11.1.min.js"><\/script>');</script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
		<?php wp_footer(); ?>
	</body>
</html>
<!-- <?php echo get_num_queries(); ?>q / <?php timer_stop(1); ?>s -->