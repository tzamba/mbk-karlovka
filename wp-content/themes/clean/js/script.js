/* SLIDER */
(function(e){var t={init:function(t){var n=e.extend({pause:true,arrows:["",""],bullets:false,interval:6e3,slides:".feature",callback:false},t);return this.each(function(t){var r=e(this),i=r.data("slide");if(i!==undefined)return;slides=r.find(n.slides),size=slides.size(),current=0;i={settings:n,slides:slides,size:size,current:current};r.data("slide",i).trigger("init.slide").slide("decorate").slide("move",0).slide("resume").hover(function(){e(this).addClass("hover").slide("pause")},function(){e(this).removeClass("hover").slide("resume")})})},move:function(t){return this.each(function(){var n=e(this),r=n.data("slide"),i=r.slides,s=r.settings.callback;r.current=t;if(r.bullets!==undefined)r.bullets.find("a").eq(t).addClass("active").siblings().removeClass("active");var o=i.removeClass("previous active next").eq(t).addClass("active"),u=i.eq(t===0?r.size-1:t-1).addClass("previous"),a=i.eq(t===r.size-1?0:t+1).addClass("next");n.toggleClass("first",t===0).toggleClass("last",t===r.size-1).data("slide",r).trigger("move.slide");if(s!==false)s(n,t,size,o,u,a)})},next:function(){return this.each(function(){var t=e(this),n=t.data("slide");index=n.current+1;if(index===n.size)index=0;t.trigger("next.slide").slide("move",index)})},previous:function(){return this.each(function(){var t=e(this),n=t.data("slide");index=n.current-1;if(index<0)index=n.size-1;t.trigger("previous.slide").slide("move",index)})},resume:function(){return this.each(function(){var t=e(this),n=t.data("slide");if(n.interval===undefined||n.settings.pause){clearInterval(n.interval);n.interval=setInterval(function(){t.slide("next")},n.settings.interval)}})},pause:function(){return this.each(function(){var t=e(this),n=t.data("slide");if(n.settings.pause)clearInterval(n.interval)})},decorate:function(){return this.each(function(){var t=e(this),n=t.data("slide"),r=e('<div class="controls"></div>');if(n.settings.arrows!==false){arrows=n.settings.arrows;n.arrows=e('<div class="arrows"><a class="previous" href="#">'+arrows[0]+'</a><a class="next" href="#">'+arrows[1]+"</a></div>").appendTo(r)}if(n.settings.bullets!==false){bullets="";for(i=1;i<size+1;i++)bullets+='<a class="bullet" href="#">'+n.settings.bullets.replace("$",i)+"</a>";n.bullets=e('<div class="bullets">'+bullets+"</div>").appendTo(r)}t.append(r).data("slide",n).find(".controls a").click(function(t){var n=e(this),r=n.parents(".controls").parent(),i=r.data("slide");t.preventDefault();if(n.hasClass("bullet"))r.slide("move",i.bullets.find("a").index(n));if(n.hasClass("previous"))r.slide("previous");if(n.hasClass("next"))r.slide("next")})})},destroy:function(){return this.each(function(){var t=e(this),n=t.data("slide");t.trigger("destroy.slide").removeData("slide").removeClass("first last hover").off(".slide");n.slides.removeClass("previous next active");n.arrows.remove();n.bullets.remove()})}};e.fn.slide=function(e){if(t[e])return t[e].apply(this,Array.prototype.slice.call(arguments,1));else if(typeof e==="object"||!e)return t.init.apply(this,arguments)}})(jQuery);

$(function() {
	/* SLIDER */
	$('#top-news-slider').slide({
		pause: false,
		arrows: false,
		bullets: 'X',
		interval: 7000,
		slides: '.top-news-slide',
		callback: function(a, b, c) {
			var slides  = $(a).find('.top-news-slide'),
				strips  = $('.top-news-strip'),
				bullets = $('.bullet');

			slides.each(function(i) {
				var slide  = $(this),
					strip  = $(strips[i]),
					bullet = $(bullets[i]);

				strip.toggleClass('active', slide.hasClass('active'));
			});
		}
	});
	var strips  = $('.top-news-strip'),
		bullets = $('.bullet');

	strips.each(function(i) {
		var strip  = $(strips[i]),
			bullet = $(bullets[i]);

		strip.on('click', function() {
			bullet.trigger('click');
			console.log('asdasd');
		});
	});

	/* TABS */
	initializeTabs();
	function initializeTabs() {
		$('.tabs .tab-header').each(function() {
			if ($(this).hasClass('active')) {
				$(this).parents('.tabs').find($(this).attr('data-tab')).addClass('active');
			};
		});
	}
	$('.tabs .tab-header').on('click', function(e) {
		e.preventDefault();
		var id = $(this).attr('data-tab'),
			tabs = $(this).parents('.tabs');

		tabs.find('.tab-header').removeClass('active');
		$(this).addClass('active');

		tabs.find('.tab-item').removeClass('active');
		tabs.find(id).addClass('active');
	});

});