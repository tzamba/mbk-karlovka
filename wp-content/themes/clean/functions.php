<?php
// CATEGORIES
define('CAT_AKTUALITY', 10);
define('CAT_KONFERENCIE', 15);
define('CAT_WHITE_PAPER', 16);
define('CAT_NAZOROVY_CLANOK', 18);
define('CAT_ODBORNY_CLANOK', 19);
define('CAT_BIZNIS_CLANOK', 20);

// POST TYPES
define('PTYPE_AUTOR', 'autor');
define('PTYPE_REFERENCIE', 'references');
define('PTYPE_SUCCESS_STORY', 'success-story');
define('PTYPE_TOP_TEMY', 'top-themes');
define('PTYPE_KONFERENCIE', 'conferences');
define('PTYPE_FEATURE', 'feature');
define('PTYPE_POST', 'post');
define('PTYPE_PAGE', 'page');

add_action('after_setup_theme', 'mbk_after_setup_theme');
if (!function_exists('mbk_after_setup_theme')) {
	function mbk_after_setup_theme() {
		load_theme_textdomain( 'mbk', get_template_directory() . '/languages' );

		mbk_clean_header();
		mbk_remove_texturize();
		mbk_register_nav_menus();
		mbk_register_sidebars();
		mbk_clean_markup();
		mbk_load_assets();
		// enable set featured image
		add_theme_support('post-thumbnails'); 
	}
}

if (!function_exists('mbk_clean_header')) {
	function mbk_clean_header() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'index_rel_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'feed_links_extra', 3);
// 		remove_action('wp_head', 'wp_print_styles', 8, 0);
		remove_action('wp_head', 'start_post_rel_link', 10, 0);
		remove_action('wp_head', 'parent_post_rel_link', 10, 0);
		remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
	}
}

if (!function_exists('mbk_remove_texturize')) {
	function mbk_remove_texturize() {
		remove_filter('category_description', 'wptexturize');
		remove_filter('list_cats', 'wptexturize');
		remove_filter('comment_author', 'wptexturize');
		remove_filter('comment_text', 'wptexturize');
		remove_filter('single_post_title', 'wptexturize');
		remove_filter('the_title', 'wptexturize');
		remove_filter('the_content', 'wptexturize');
		remove_filter('the_excerpt', 'wptexturize');
	}
}

if (!function_exists('mbk_register_nav_menus')) {
	function mbk_register_nav_menus() {
		register_nav_menus(
			array(
				'header-menu' => __( 'Header menu', 'mbk' ),
				'footer-menu' => __( 'Footer menu', 'mbk' )
			)
		);
	}
}

if (!function_exists('mbk_register_sidebars')) {
	function mbk_register_sidebars() {
		if (function_exists('register_sidebar')) {
			register_sidebar(
				array(
					'id' => 'sidebar',
					'name' => __( 'Sidebar', 'mbk' ),
					'before_widget' => '<aside id="%1$s" class="aside %2$s">',
					'after_widget' => '</aside>',
					'before_title' => '<header class="header"><h1 class="title">',
					'after_title' => '</h1></header>'
				)
			);
		}
	}
}

if (!function_exists('mbk_clean_markup')) {
	function mbk_clean_markup() {
		add_filter('wp_list_categories', 'mbk_clean_category_list_rel');
		add_filter('the_category', 'mbk_clean_category_list_rel');
		// odstrani automaticke pridavenie <p> tagov
		remove_filter('the_content', 'wpautop');
		if (!defined('WP_ADMIN') && defined('WP_USE_THEMES')) {
			ob_start('mbk_clean_markup_start');
			add_action('shutdown', 'mbk_clean_markup_flush');
		}
	}
}

if (!function_exists('mbk_clean_category_list_rel')) {
	function mbk_clean_category_list_rel($text) {
		$text = str_replace('rel="category tag"', 'rel="tag"', $text);
		$text = str_replace('rel="category"', 'rel="tag"', $text);
		return $text;
	}
}

if (!function_exists('mbk_clean_markup_start')) {
	function mbk_clean_markup_start($buffer) {
		$buffer = mbk_clean_quotes($buffer);
		$buffer = mbk_clean_slash($buffer);
		return $buffer;
	}
}

if (!function_exists('mbk_clean_quotes')) {
	function mbk_clean_quotes($buffer) {
		preg_match_all("~<[a-z]+[^<>]*='[^<>]*>~i", $buffer, $matched_tags);

		foreach ($matched_tags[0] as $tag) {
			unset($converted_tag);
			preg_match_all("~\s([a-z]+)='(.*?)'~", $tag, $matched_attributes);

			foreach ($matched_attributes[0] as $key => $attributes_string) {
				if (isset($matched_attributes[1][$key], $matched_attributes[2][$key])) {
					if (!isset($converted_tag)) $converted_tag = $tag;
					$converted_tag = str_replace(trim($attributes_string), $matched_attributes[1][$key].'="'.$matched_attributes[2][$key].'"', $converted_tag);
				}
			}

			if (isset($converted_tag)) $buffer = str_replace($tag, $converted_tag, $buffer);
		}

		return $buffer;
	}
}

if (!function_exists('mbk_clean_slash')) {
	function mbk_clean_slash($buffer) {
		return preg_replace('/(<.+)\s\/>/', '$1>', $buffer);
	}
}

if (!function_exists('mbk_clean_markup_flush')) {
	function mbk_clean_markup_flush() {
	    ob_end_flush();
	}
}

if (!function_exists('mbk_load_assets')) {
	function mbk_load_assets() {
		$version = 1;
		if (!is_admin()) {
			//wp_deregister_script('l10n');
			//wp_deregister_script('jquery');

			wp_register_style('posam-style', get_template_directory_uri() . '/style.css', array(), $version, 'all');
			wp_enqueue_style('posam-style');
		}
	}
}
add_action( 'wp_enqueue_scripts', 'mbk_load_assets' );

if (!function_exists('mbk_comment_callback')) {
	function mbk_comment_callback($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		echo '<li><article id="comment-' . get_comment_ID() . '" ' . comment_class('comment', null, null, false) . '><header class="header"><span class="author>Comment by <cite>' . get_avatar($comment, 48, null, get_comment_author()) . get_comment_author_link() . '</cite></span> <span class="posted">on <time datetime="' . esc_attr(get_comment_time('c')) . '">' . get_comment_date() . ' ' . get_comment_time() . '</time></span>' . mbk_get_edit_comment_link(__( 'Edit', 'mbk' )) . ' ' . mbk_get_comment_reply_link(array_merge($args, array('reply_text' => __( 'Reply', 'mbk' ), 'depth' => $depth, 'max_depth' => $args['max_depth']))) . '</header><div class="content">' . mbk_get_comment_content($comment, $args, $depth) . '</div></article>';
	}
}

if (!function_exists('mbk_get_comment_content')) {
	function mbk_get_comment_content($comment, $args, $depth) {
		$html = '';

		if ($comment->comment_type == 'pingback' || $comment->comment_type == 'trackback') $html .= sprintf('<p>%1$s %2$s</p>', __( 'Pingback:', 'mbk' ), get_comment_author_link());
		else $html .= '<p>' . get_comment_text() . '</p>';

		if ($comment->comment_approved == '0') $html .= sprintf('<p class="moderation">%s</p>', __( 'Your comment is awaiting moderation.', 'mbk' ));

		return '<div class="content">' . $html . '</div>';
	}
}

if (!function_exists('mbk_get_edit_comment_link')) {
	function mbk_get_edit_comment_link($link = null, $before = '', $after = '') {
		global $comment;

		if ( !current_user_can('edit_comment', $comment->comment_ID)) return;
		if (null === $link) $link = __( 'Edit This', 'mbk' );

		$link = '<a class="comment-edit-link" href="' . get_edit_comment_link($comment->comment_ID) . '" title="' . esc_attr__($link) . '">' . $link . '</a>';
		return $before . apply_filters('edit_comment_link', $link, $comment->comment_ID) . $after;
	}
}

if (!function_exists('mbk_get_comment_reply_link')) {
	function mbk_get_comment_reply_link($args = array(), $comment = null, $post = null) {
		global $user_ID;

		$defaults = array('add_below' => 'comment', 'respond_id' => 'respond', 'reply_text' => __('Reply', 'mbk'), 'login_text' => __('Log in to Reply', 'mbk'), 'depth' => 0, 'before' => '', 'after' => '');

		$args = wp_parse_args($args, $defaults);

		if (0 == $args['depth'] || $args['max_depth'] <= $args['depth']) return;

		extract($args, EXTR_SKIP);

		$comment = get_comment($comment);
		if ( empty($post) ) $post = $comment->comment_post_ID;

		$post = get_post($post);
		if ( !comments_open($post->ID) ) return false;

		$link = '';

		if (get_option('comment_registration') && !$user_ID) $link = '<a rel="nofollow" class="comment-reply-login" href="' . esc_url(wp_login_url(get_permalink() ) ) . '">' . $login_text . '</a>';
		else $link = '<a class="comment-reply-link" href="' . esc_url(add_query_arg('replytocom', $comment->comment_ID)) . '#' . $respond_id . '" onclick="return addComment.moveForm(\'' . $add_below . '-' . $comment->comment_ID . '\', \'' . $comment->comment_ID . '\', \'' . $respond_id . '\', \'' . $post->ID . '\');">' . $reply_text . '</a>';
		return apply_filters('comment_reply_link', $before . $link . $after, $args, $comment, $post);
	}
}


function mbk_wp_title( $title, $sep ) {
	if (is_feed()) {
		return $title;
	}
	global $page, $paged;

	$title .= get_bloginfo('name', 'display');

	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) {
		$title .= " $sep $site_description";
	}

	if (($paged >= 2 || $page >= 2) && !is_404()) {
		$title .= " $sep " . sprintf(__('Page %s', '_s'), max($paged, $page));
	}

	return $title;
}
add_filter( 'wp_title', 'mbk_wp_title', 10, 2 );


if (!function_exists('mbk_comment_navigation')) {
	function mbk_comment_navigation() {
		add_filter('previous_comments_link_attributes', 'mbk_comments_link_attributes');
		add_filter('next_comments_link_attributes', 'mbk_comments_link_attributes');

		$previous = get_previous_comments_link(__('Previous comments', 'mbk'));
		$next = get_next_comments_link(__('Next comments', 'mbk'));

		if ($previous || $next) {
			echo '<nav class="move move-comments"><ul>';

			if ($previous) echo '<li class="previous">' . $previous . '</li>';
			if ($next) echo '<li class="next">' . $next . '</li>';

			echo '</ul></nav>';
		}
	}
}

if (!function_exists('mbk_comments_link_attributes')) {
	function mbk_comments_link_attributes() {
		return '';
	}
}

if (!function_exists('mbk_comment_form')) {
	function mbk_comment_form($args = array(), $post_id = null) {
		global $user_identity, $id;

		if (null === $post_id) $post_id = $id;
		else $id = $post_id;

		$commenter = wp_get_current_commenter();

		$req = (get_option('require_name_email')) ? __(' (required)', 'mbk') : '';
		$fields = array(
				'author' => '<label class="author"><input id="author" placeholder="' . __('Name', 'mbk') . ' ' . $req . '" type="text" name="author"' . ((!empty($commenter['comment_author'])) ? ' value="' . esc_attr($commenter['comment_author']) . '"' : '') . '></label>',
				'email' => '<label class="email"><input id="email" placeholder="' . __('Email', 'mbk') . ' ' . $req . '" type="text" name="email"' . ((!empty($commenter['comment_author_email'])) ? ' value="' . esc_attr($commenter['comment_author_email']) . '"' : '') . '></label>',
				'url' => '<label class="url"><input id="url" placeholder="' . __('Website', 'mbk') . '" type="text" name="url"' . ((!empty($commenter['comment_author_url'])) ? ' value="' . esc_attr($commenter['comment_author_url']) . '"' : '') . '></label>',
		);

		$defaults = array(
				'fields' => apply_filters('comment_form_default_fields', $fields),
				'comment_field' => '<label class="comment"><textarea id="comment" placeholder="' . __('Comment', 'mbk') . '" name="comment"></textarea></label>',
				'id_form' => 'respond',
				'id_submit' => 'submit',
				'title_reply' => __('Respond', 'mbk'),
				'title_reply_to' => __('Respond to %s', 'mbk'),
				'cancel_reply_link' => __('Cancel', 'mbk'),
				'label_submit' => __('Submit comment', 'mbk'),
				'must_log_in' => '<p class="must-login">' . sprintf(__('You must be <a href="%s">logged in</a> to post a comment.', 'mbk'), wp_login_url(apply_filters('the_permalink', get_permalink($post_id)))) . '</p>',
				'logged_in_as' => '<p class="logged-in-as">' . sprintf(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'mbk'), admin_url('profile.php'), $user_identity, wp_logout_url(apply_filters('the_permalink', get_permalink($post_id)))) . '</p>',
				'comment_notes_before' => '<p class="comment-notes">' . __('Your email address will not be published.', 'mbk') . ($req ? $required_text : '') . '</p>',
				'comment_notes_after'  => '<p class="allowed-tags">' . sprintf(__('You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', 'mbk'), ' <code>' . allowed_tags() . '</code>' ) . '</p>'
		);

		$args = wp_parse_args($args, apply_filters('comment_form_defaults', $defaults));

		if (comments_open()) {
			echo '<form id="' . esc_attr($args['id_form']) . '" class="form" action="' . site_url('/wp-comments-post.php') . '" method="post">';

			if (get_option('comment_registration') && !is_user_logged_in()) {
				echo $args['must_log_in'];
				do_action('comment_form_must_log_in_after');
			} else {
				if (is_user_logged_in()) {
					echo apply_filters('comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity);
					do_action('comment_form_logged_in_after', $commenter, $user_identity);
				}

				do_action('comment_form_top');

				echo '<fieldset class="fields">';

				if (!is_user_logged_in()) {
					do_action('comment_form_before_fields');
					foreach ((array) $args['fields'] as $name => $field) echo apply_filters("comment_form_field_{$name}", $field);
					do_action('comment_form_after_fields');
				}

				echo apply_filters('comment_form_field_comment', $args['comment_field']);

				echo '</fieldset><fieldset class="buttons"><input id="' . esc_attr($args['id_submit']) . '" class="button" type="submit" name="submit" value="' . esc_attr($args['label_submit']) . '">' . get_cancel_comment_reply_link($args['cancel_reply_link']) . '</fieldset>';

				comment_id_fields($post_id);

				do_action('comment_form', $post_id);
				do_action('comment_form_after');

				echo $args['comment_notes_before'] . ' ' . $args['comment_notes_after'];
			}

			echo '</form>';
		} else {
			do_action('comment_form_comments_closed');
		}
	}
}

add_filter( 'request', 'mbk_request_filter' );
function mbk_request_filter( $query_vars ) {
	if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
		$query_vars['s'] = " ";
	}
	return $query_vars;
}

function mbk_excerpt_more($more) {
	global $post;
	return '... <a href="'. get_permalink($post->ID) . '">Viac</a>';

}
add_filter('excerpt_more', 'mbk_excerpt_more');

if (!function_exists('mbk_body_class')) {
	function mbk_body_class() {
		global $wp_registered_sidebars;

		foreach ($wp_registered_sidebars as $sidebar) {
			$id = $sidebar['id'];
			if (is_active_sidebar($id)) $classes[] = 'sidebar-' . $id;
		}

		if (get_option('show_avatars')) $classes[] = 'comment-avatars';

		echo 'class="' . join(' ', get_body_class($classes)) . '"';
	}
}

function get_file_extension($file) {
	return strtolower(substr(strrchr($file,'.'),1));
}

function display_breadcrumbs() {
	echo '<nav class="breadcrumbs">';
	if(function_exists('bcn_display')) {
		bcn_display();
	}
	echo '</nav>';
}

function get_top_post_id() {
	global $post;
	$ancestors = get_post_ancestors($post->ID);
	if (empty($ancestors)) {
		return $post->ID;
	}
	return end($ancestors);
}

function get_subtitle() {
	$id = get_top_post_id();
	$top_post = get_page($id);
	echo $top_post->post_title;
}

function get_theme_location() {
	$id = get_top_post_id();
	switch ($id) {
		// 44 - Riesenia a sluzby
		case 44:
		case 84:
			$themeLocation = 'riesenia-a-sluzby';
			break;
		// 44 - Segmenty
		case 55:
			$themeLocation = 'segmenty';
			break;
		// 260 - Thinking
		case 955:
			$themeLocation = 'thinking';
			break;
		// 262 - O spolocnosti
		case 262:
			$themeLocation = 'o-spolocnosti';
			break;
		default:
			$themeLocation = '';
			break;
	}
	return $themeLocation;
}

function get_case_study($cases) {
	$echo = "";
	if (is_front_page()) {
		$tags = '';
	} else if (is_array($cases)) {
		$tags = $cases;
	} else {
		$tags = get_page_name();
	}
	$wp_query = new WP_Query( array( 'post_type' => PTYPE_SUCCESS_STORY, 'posts_per_page' => ((is_front_page()) ? 4 : 10), 'order' => 'ASC', 'tag_slug__in' => $tags) );
	if ( $wp_query->have_posts() ) {
		$echo .= '<div class="cases">';
		$echo .= '<h2 class="title">'.__( 'Selected case studies', 'mbk' ).'</h2>';
		$echo .= '<div class="slides">';
		while ( $wp_query->have_posts() ) :
			$wp_query->the_post();
			$echo .= '<div class="slide">';
			$echo .= '<p>'.get_the_content().'</p>';
			$echo .= '</div>';
		endwhile;
		$echo .= '</div>';
		$echo .= '</div>';
	}
	wp_reset_query();
	return $echo;
}

function get_features() {
	$query = new WP_Query( array( 'post_type' => PTYPE_FEATURE, 'posts_per_page' => 10, 'orderby' => 'date' ) );
	while ( $query->have_posts() ) :
		$query->the_post();
		$display = types_render_field("display", array());
		if (isset($display) && strlen($display) > 0) {
			echo $query->post->post_content;
		}
	endwhile;
	wp_reset_query();
}

function add_class_attachment_link($html){
	$postid = get_the_ID();
	$html = str_replace('<a','<a class="button"',$html);
	return $html;
}

function get_author_info() {
	$author = types_render_field("author", array());
	if (isset($author) && strlen($author) > 0) {
		echo $author;
	}
}

function get_top_themes() {
	$title = types_render_field("orientation", array());
	if (isset($title) && strlen($title) > 0) {
		echo '<h3 class="title">' . __( 'More about product: ', 'mbk' ) . '</h3><p>' . $title . '</p>';
	}
}

function get_extended_title() {
	$title = types_render_field("extended-title", array());
	if (isset($title) && strlen($title) > 0) {
		echo '<p class="perex">';
		echo $title;
		echo '</p>';
	}
}

function get_page_name() {
	$page = get_page(get_the_ID());
	$pageName = $page->post_name;
	return $pageName;
}

function get_right_sidebar() {
	echo '<div class="aside">';
	$right = types_render_field("right-sidebar", array());
	if (isset($right) && strlen($right) > 0) {
		echo $right;
	}

	$referencie = get_vybrane_referencie();
	if (strlen($referencie) > 0) {
		echo '<h3 class="title">'.__( 'Selected references', 'mbk' ).'</h3>';
		echo $referencie;
	}

	$temy  = '';
	$temy .= get_suvisiace_temy(__( 'Top themes', 'mbk' ) , PTYPE_TOP_TEMY);
	$temy .= get_suvisiace_temy(__( 'News', 'mbk' ), PTYPE_POST);
	$temy .= get_suvisiace_temy(__( 'Conferences', 'mbk' ) , PTYPE_KONFERENCIE);
	if (strlen($temy) > 0) {
		echo '<p></p><hr><h3 class="title">'.__( 'Related links', 'mbk' ).'</h3>';
		echo $temy;
	}
}

function get_suvisiace_temy($title, $categories) {
	$echo = '';
	$wp_query = new WP_Query( array( 'posts_per_page' => 10, 'post_type' => $categories, 'orderby' => 'date', 'tag' => get_page_name(), 'posts per page' => 10 ) );
	if ($wp_query->have_posts()) {
		$echo .= '<p><strong>'.$title.'</strong><ul>';
		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();
			if ($categories == PTYPE_TOP_TEMY) {
				// ATTACHEMENTS
				$args = array(
						'post_type' => 'attachment',
						'post_mime_type' => 'application/pdf,application/msword',
						'numberposts' => null,
						'post_status' => null,
						'post_parent' => $wp_query->post->ID
				);
				$attachments = get_posts($args);
				if ($attachments) {
					foreach ($attachments as $attachment) {
						$echo .= '<li>'.wp_get_attachment_link($attachment->ID, 'thumbnail', false, false, get_the_title()).'</li>';
					}
			}
			} else {
				$echo .= '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
			}
		}
		$echo .= '</ul></p>';
		wp_reset_query();
		return $echo;
	}
	wp_reset_query();
	return '';
}

function get_vybrane_referencie() {
	$echo = '';
	$ID = get_the_ID();
	$referencie = get_post_meta($ID, 'reference_post_class', true);
	if ($referencie) {
		$echo .= '<ul>';
		foreach ($referencie as $ref) {
			$args = array(
					'post_type' => 'attachment',
					'numberposts' => null,
					'post_status' => null,
					'post_parent' => $ref
			);

			$attachments = get_posts($args);
			$echo .= '<li>';
			$page = get_page($ref);
			$echo .= $page->post_title;
			if ($attachments) {
				$echo .= '<br>';
				foreach ($attachments as $attachment) {
					$echo .= wp_get_attachment_link($attachment->ID, 'thumbnail', false, true);
				}
			}
			$echo .= '</li>';
		}
		$echo .= '</ul>';
	}

	return $echo;
}

add_filter( 'icon_dir', 'mbk_img_directory' );
add_filter( 'icon_dir_uri', 'mbk_img_uri' );

function mbk_img_directory( $icon_dir ) {
	return get_stylesheet_directory() . '/img/icon';
}

function mbk_img_uri( $icon_dir ) {
	return get_stylesheet_directory_uri() . '/img/icon';
}

function get_address() {
	$address = types_render_field("address", array());
	if (isset($address) && strlen($address) > 0) {
		echo '<div class="box">';
		echo $address;
		echo '</div>';
	}
}

function get_subor() {
	$file = types_render_field("subor", array());
	if (isset($file) && strlen($file) > 0) {
		return $file;
	}
	return null;
}

function get_top_new() {
	echo '<div class="column new">';
	echo '<h2 class="title">'.__( 'Top themes', 'mbk' ).'</h2>';
	$query = new WP_Query( array( 'post_type' => PTYPE_TOP_TEMY, 'posts_per_page' => 1, 'orderby' => 'date') );
	while ( $query->have_posts() ) :
		$query->the_post();
		$args = array(
				'post_type' => 'attachment',
				'numberposts' => null,
				'post_status' => null,
				'post_parent' => $query->post->ID
		);
		$attachments = get_posts($args);
		if ($attachments) {
			foreach ($attachments as $attachment) {
				echo '<h3>'.wp_get_attachment_link($attachment->ID, 'thumbnail', false, false, get_the_title()).'</h3>';
			}
		}
		echo the_excerpt();
		echo '<small>'.strtolower(get_the_date('d. F Y')).'</small>';
	endwhile;
	wp_reset_query();
	echo '<br><a href="/thinking/top-temy/">&rarr; Ďalšie top témy</a>';
	echo '</div>';
}

function get_top_news() {
	echo '<div class="column news">';
	echo '<h2 class="title">'.__( 'News', 'mbk' ).'</h2>';
	echo '<ul>';
	$query = new WP_Query( array( 'category__in' => CAT_AKTUALITY, 'posts_per_page' => 3, 'orderby' => 'date') );
	while ( $query->have_posts() ) :
		$query->the_post();
		echo '<li>';
		echo '<h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>';
		echo '<small>'.strtolower(get_the_date('d. F Y')).'</small>';
		echo '</li>';
	endwhile;
	wp_reset_query();
	echo '</ul>';
	echo '</div>';
}

function get_mbk_lat() {
	$lat = types_render_field("latitude", array());
	echo (isset($lat) && strlen($lat) > 0) ? $lat : '48.177653';
}

function get_mbk_lon() {
	$lon = types_render_field("longitude", array());
	echo (isset($lon) && strlen($lon) > 0) ? $lon : '17.145023';
}

function display_attachements( $content ) {
	global $post;

	if ( $post->post_type == 'page' && $post->post_status == 'publish' ) {
		$attachments = get_posts( array(
				'post_type' => 'attachment',
				'post_parent' => $post->ID
		) );

		// CI CHCEME NA DANEJ STRANKE ZOBRAZIT PRILOHY
		$displayAttachements = types_render_field("attachements", array());
		if ( (isset($displayAttachements) && $displayAttachements == 'yes') && $attachments ) {
			$content .= '<article><p>';
			$content .= '<div class="attachments"><div>';
			foreach ( $attachments as $attachment ) {
				$title = wp_get_attachment_link( $attachment->ID, false );
				$img = wp_get_attachment_image($attachment->ID, 'thumbnail', true);
				$content .= $img . ' ' .$title;
				$content .= (isset($br)) ? $br : $br = '<br><br>';
			}
			$content .= '<div></div>';
			$content .= '</p></article>';
		}
	}

	return $content;
}

function filesize2human ($num) {
	$filesize_tmp1=$num/1024;
	if ($filesize_tmp1<1024): $filesize=Round($filesize_tmp1,2).' KB';
	else: $filesize=Round(($filesize_tmp1/1024),2).' MB';
	endif;
	return str_replace(".",",",$filesize);
}
