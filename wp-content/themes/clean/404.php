<?php get_header(); ?>
<div id="subtitle">
	<div class="wrapper">
		<h2><?php _e( 'Page not found', 'posam'); ?></h2>
	</div>
</div>
<div id="content">
	<div class="wrapper">
		<div id="side">
			<div class="error">
				<img src="<?php echo get_template_directory_uri(); ?>/img/error404.png" alt="Error 404">
			</div>
		</div>
		<div id="main">
			<article class="article article-wide">
				<div class="content">
					<p>Ľutujeme, no stránka, ktorú hľadáte, nebola nájdená. Skontrolujte prosím, či ste správne zadali adresu stránky (napr. www.posam.sk/kontakt/). Ak je adresa správna, tak bola táto stránka odstránená alebo presunutá pod inú adresu. Pre získanie informácií, ktoré hľadáte, skúste použiť jedno z nasledovných riešení:</p>
					<ul>
						<li>Použite vyhľadávanie</li>
						<li>Použite hlavnú hornú navigáciu</li>
						<li>Prejdite na <a href="/sitemap_index.xml">mapu stránok</a></li>
						<li>Prejdite na <a href="/">úvodnú stránku</a></li>
						<li>Vrátťe sa na <a href="#" onclick="window.history.back();">predchádzajúcu stránku</a></li>
					</ul>
					<p>Ak by sa vám nepodarilo nájsť hľadané informácie alebo by technické problémy pretrvávali, <a href="/kontakt">kontaktujte nás</a>, prosím.</p>
				</div>
			</article>
		</div>
<?php get_footer(); ?>