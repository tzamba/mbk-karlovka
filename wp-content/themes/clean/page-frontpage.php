<?php
/*
Template Name: Frontpage
*/
	get_header();
?>
		<div id="main" class="section"><div class="wrapper">
			<div class="section-row">
				<div class="t-third"><div class="wrapper">
					<div id="top-news">
						<div id="top-news-slider">
							<div class="top-news-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/img/img-news.png">
								<div class="title-wrapper">
									<a href="#" class="title">
										<h3>Mladými hráčmi roka Kováč a Sabó</h3>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
									</a>
								</div>
							</div>
							<div class="top-news-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/img/img-news.png">
								<div class="title-wrapper">
									<a href="#" class="title">
										<h3>Mladými hráčmi roka Kováč a Sabó</h3>
										<p>Lorem ipsum dolor sit amet, consectetuer Kováč a Sabópiscing Kováč a Sabó Kováč a Sabópiscing Kováč a Sabó adipiscing elit</p>
									</a>
								</div>
							</div>
							<div class="top-news-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/img/img-news.png">
								<div class="title-wrapper">
									<a href="#" class="title">
										<h3>Mladými hráčmi roka Kováč a Sabó  Kováč a Sabó Kováč a Sabó</h3>
										<p>Lorem ipsum dolor sit amet, consectetuer adi  Kováč a Sabó Kováč a Sabó Kováč a Sabópiscing elit</p>
									</a>
								</div>
							</div>
							<div class="top-news-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/img/img-news.png">
								<div class="title-wrapper">
									<a href="#" class="title">
										<h3>Mladými hráčmi roka</h3>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
									</a>
								</div>
							</div>
							<div class="top-news-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/img/img-news.png">
								<div class="title-wrapper">
									<a href="#" class="title">
										<h3>Mladými hráčmi roka Kováč a Sabó</h3>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
									</a>
								</div>
							</div>
						</div>
						<div id="top-news-strips">
							<a href="#" class="top-news-strip">
								<table>
									<tr>
										<td><img src="<?php echo get_template_directory_uri(); ?>/img/img-news-strip.png"></td>
										<td><p>Strapo vystup na  o 15:00</p></td>
									</tr>
								</table>
							</a>
							<a href="#" class="top-news-strip">
								<table>
									<tr>
										<td><img src="<?php echo get_template_directory_uri(); ?>/img/img-news-strip.png"></td>
										<td><p>koncerte na Grape </p></td>
									</tr>
								</table>
							</a>
							<a href="#" class="top-news-strip">
								<table>
									<tr>
										<td><img src="<?php echo get_template_directory_uri(); ?>/img/img-news-strip.png"></td>
										<td><p>Grape Festivale o 15:00</p></td>
									</tr>
								</table>
							</a>
							<a href="#" class="top-news-strip">
								<table>
									<tr>
										<td><img src="<?php echo get_template_directory_uri(); ?>/img/img-news-strip.png"></td>
										<td><p>Strapo vystup na Grape Festivale o 15:00</p></td>
									</tr>
								</table>
							</a>
							<a href="#" class="top-news-strip">
								<table>
									<tr>
										<td><img src="<?php echo get_template_directory_uri(); ?>/img/img-news-strip.png"></td>
										<td><p>Strapo  o 15:00</p></td>
									</tr>
								</table>
							</a>
						</div>
					</div>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
				</div></div>
			</div>

			<div class="section-row">
				<div class="third"><div class="wrapper">
					<h2>Podujatia</h2>
					<div class="tabs">
						<div class="half tab-header active" data-tab="#event-first"><div class="wrapper">
							<a href="#">Najbližšie</a>
						</div></div>
						<div class="half tab-header" data-tab="#event-second"><div class="wrapper">
							<a href="#">Posledné</a>
						</div></div>
						<div id="event-first" class="tab-item"><div class="wrapper">
							<table class="bordered">
								<tbody>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
								</tbody>
							</table>
						</div></div>
						<div id="event-second" class="tab-item"><div class="wrapper">
							<table class="bordered">
								<tbody>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
								</tbody>
							</table>
						</div></div>
					</div>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Kalendár</h2>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Správy</h2>
					<div class="tabs">
						<div class="half tab-header active" data-tab="#news-first"><div class="wrapper">
							<a href="#">Bleskovky</a>
						</div></div>
						<div class="half tab-header" data-tab="#news-second"><div class="wrapper">
							<a href="#">Spod košov</a>
						</div></div>
						<div id="news-first" class="tab-item"><div class="wrapper">
							<table class="bordered">
								<tbody>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
								</tbody>
							</table>
						</div></div>
						<div id="news-second" class="tab-item"><div class="wrapper">
							<table class="bordered">
								<tbody>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
									<tr><td>18. 09 o 18:30<br><strong>BK Inter BA - MBK KARLOVKA</strong></td></tr>
								</tbody>
							</table>
						</div></div>
					</div>
				</div></div>
			</div>
			
			<div class="section-row">
				<div class="third"><div class="wrapper">
					<h2>Galéria</h2>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Facebook</h2>
				</div></div>
				<div class="third"><div class="wrapper">
					<h2>Youtube</h2>
				</div></div>
			</div>
			
			<div class="section-row">
				<div class="third"><div class="wrapper">
					<h2>FunShop</h2>
				</div></div>
				<div class="third banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
				</div></div>
				<div class="third banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-01.png">
				</div></div>
			</div>
			
			<div class="section-row">
				<div class="full banner"><div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-02.png">
				</div></div>
			</div>
		</div></div>

		<div id="partners" class="section"><div class="wrapper">

		</div></div>
<?php get_footer(); ?>