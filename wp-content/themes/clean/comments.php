<?php
if (have_comments()) {
	echo '<div id="comments">';

	if (post_password_required()) echo '<p class="nopassword">' . __('This post is password protected. Enter the password to view any comments.', 'posam') . '</p>';
	elseif (have_comments()) {
		echo '<ul class="comments">';
		wp_list_comments(array('callback' => 'posam_comment_callback'));
		echo '</ul>';
		posam_comment_navigation();
	}
	elseif (!comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) echo '<p class="nocomments">' . __('Comments are closed.', 'posam') . '</p>';

	echo '</div>';
}

posam_comment_form(array('comment_notes_before' => null, 'comment_notes_after' => null));
?>