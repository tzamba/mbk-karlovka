						<h1><?php the_title(); ?></h1>
						<div class="content-wrapper">
							<?php echo wpautop(apply_filters('the_content', $post->post_content)); ?>
							<div class="meta"><?php edit_post_link(__('Edit', 'posam'), ' '); ?></div>
						</div>