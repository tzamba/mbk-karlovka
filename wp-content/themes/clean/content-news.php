<div class="listing">
	<div class="heading">
		<h1 class="title"><?php the_title(); ?></h1>
	</div>
	<?php
		// 10  - 'aktuality'
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array( 'posts_per_page' => 5, 'paged' => $paged, 'category__in' => CAT_AKTUALITY );
		$wp_query= new WP_Query($args);

		while ( $wp_query->have_posts() ) :
		$wp_query->the_post();
		$id = $wp_query->post->ID;
	?>
		<article class="article article-horizontal">
			<header class="header">
				<h1 class="title"><a href="<?php the_permalink(); ?>"><?php echo get_the_title($id); ?></a></h1>
				<small><?php echo strtolower(get_the_date('d. F Y')); ?></small>
			</header>
			<div class="content">
				<?php get_extended_title(); ?>
			</div>
		</article>
	<?php
		endwhile;
		wp_pagenavi('<div class="pager">', '</div>');
	?>
</div>