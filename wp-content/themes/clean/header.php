<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=1024">
		<meta name="robots" content="index,follow">
		<meta name="keywords" content="MBK Karlovka, Bratislava, basketball, basketbalová liga, pohybové a loptové hry, šport">
		<meta name="author" content="Zombi | www.zombi.sk">
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="stylesheet" href="css/style.css">
		<title><?php wp_title('|', true, 'right'); ?></title>
	</head>
	<body>
		<div id="header" class="section"><div class="wrapper">
			<a href="<?php echo home_url('/'); ?>" id="logo"></a>
			<?php
				wp_nav_menu(array(
					'theme_location' => 'header-menu',
					'menu' => 'main menu',
					'menu_class' => 'top-menu',
					'echo' => true,
					'fallback_cb' => '',
					'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>'
				));
			?>
			<?php get_search_form(); ?>
		</div></div>